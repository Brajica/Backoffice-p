import { MerlimBackofficeAngularPage } from './app.po';

describe('merlim-backoffice-angular App', () => {
  let page: MerlimBackofficeAngularPage;

  beforeEach(() => {
    page = new MerlimBackofficeAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
