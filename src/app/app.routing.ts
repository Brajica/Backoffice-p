/*
 * el archivo de rutas principal (hay un archivo de rutas por cada modulo en la app)
 * todos los modulos de rutas son iguales lo unico que cambia son las rutas declaradas
 */
import { NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// declaramos las rutas
// en las rutas las variables se declaran de la manera "ruta/:variable/ruta"
export const routes: Routes = [
    // redireccionamos la base
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    // cargamos modulos segundarios
    { path: 'test', loadChildren: 'app/test/test.module#TestModule' },
    { path: 'login', loadChildren: 'app/login/login.module#LoginModule' },
    { path: 'recover-password', loadChildren: 'app/recover-password/recover-password.module#RecoverPasswordModule' },
    { path: 'register', loadChildren: 'app/register/register.module#RegisterModule' },
    { path: 'backoffice', loadChildren: 'app/backoffice/backoffice.module#BackofficeModule' }
];

/*
 * cargamos las rutas y las exportamos al modulo padre
 */
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
