import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Router} from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {CustomRequests} from '../util/services';
import {GLOBALS} from '../config/config';

@Injectable()
export class SessionService {

    constructor(private http: Http, private router: Router) {
        // injectamos librerias
    }

    public Login(user) {
        return this.http.post(GLOBALS.URL_BASE + 'login', user)
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }

    public Logout(username: string) {
        const headers = new Headers();

        CustomRequests.CreateAuthorizationHeader(headers);

        const options = new RequestOptions({headers: headers});
        
        return this.http.post(GLOBALS.URL_BASE + 'logout', {'username': username}, options)
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }

    public LoginCheck() {
        const l_token = localStorage.getItem('csrf_token');
        const isLoged = (l_token !== null && l_token !== '');
        let link = [];
        if (isLoged) {
            link = ['/backoffice/home'];
            this.router.navigate(link);
        }
    }

    public LogoutCheck() {
        const l_token = localStorage.getItem('csrf_token');
        const isLoged = (l_token !== null && l_token !== '');
        let link = [];
        if (!isLoged) {
            link = ['/login'];
            this.router.navigate(link);
        }
    }
}

export class Session {
    username: string;
    password: string;
    captcha: string;

    public Initialize(): Session {

        this.username = '';
        this.password = '';
        this.captcha = '';

        return this;
    }

    public toJson(): any {
        return {
            'username': this.username,
            'password': this.password,
            'captcha': this.captcha
        };
    }
}
