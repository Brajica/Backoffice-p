import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {CustomRequests} from '../util/services';
import {GLOBALS} from '../config/config';

@Injectable()
export class AretoService {

    constructor(private http: Http) {
        // injectamos librerias
    }
    
    public getDataAreto(username: string) {
        return this.http.post(GLOBALS.URL_BASE + 'paid/creditcard', {'username': username})
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }

    // public Logout() {
    //     const headers = new Headers();

    //     CustomRequests.CreateAuthorizationHeader(headers);

    //     const options = new RequestOptions({headers: headers});
        
    //         return this.http.post(GLOBALS.URL_BASE + 'logout', {'username': user.username}, options)
    //             .map(CustomRequests.ResponseData)
    //             .catch(CustomRequests.handleError);
    // }
    
    public sendData(data: any){
        // console.info(data)
        return this.http.post(GLOBALS.URL_BASE + 'paid/processpayment', {'expiration_date': data.expiration_date,
                                                                         'card_number': data.card_number,
                                                                         'cvc_code': data.cvc_code,
                                                                         'name_owner': data.titular,
                                                                         'postal_code': data.postal_code,
                                                                         'package_cost':data.package_cost,
                                                                         'system_activation':data.system_activation,
                                                                         'total_package':data.total_package,
                                                                         'currency_code':data.currency_code,
                                                                         'street':data.street,
                                                                         'desc': data.desc,
                                                                         'city': data.city,
                                                                         'country_code': data.country_code
        })
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }
}
