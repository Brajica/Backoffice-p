import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {CustomRequests} from '../util/services';
import {GLOBALS} from '../config/config';

@Injectable()
export class PayService {
    public lang = localStorage.getItem('lang')
    constructor(private http: Http) {
    }

    public Epin (code: string, key: string, captcha: string, user: string) {
        return this.http.post(GLOBALS.URL_BASE + this.lang + '/paid/epin', { "code": code, "key": key, "captcha": captcha, "username": user })
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }
    public EpinUpgrade(data: any){
       return this.http.post(GLOBALS.URL_BASE + this.lang + '/paid/epin/upgrade/membership', {'username': data.username,
                                                                                             'pack':data.pack, 
                                                                                             'code':data.code,
                                                                                             'key': data.key})
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }
}
