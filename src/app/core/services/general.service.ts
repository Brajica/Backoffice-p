import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {CustomRequests} from '../util/services';
import {GLOBALS} from '../config/config';

@Injectable()
export class GeneralService {
    constructor(private http: Http) {
    }
    
    public Countries () {
        return this.http.get(GLOBALS.URL_BASE + 'location/countries')
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }
}