import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {default as swal} from 'sweetalert2';

export class SWAlert {

    constructor(private ts: TranslateService,
                private router: Router) {

    }

    public SWAlertSuccessLink(objalert: string, route: any) {
        this.ts.get('General').subscribe((general: any) => {
            this.ts.get('SWA.' + objalert).subscribe((resalert: any) => {
                swal({
                    text: resalert,
                    type: 'success',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: general.ok,
                    confirmButtonClass: 'pink darken-3 white-text '
                }).then(() => {
                    this.router.navigate(route);
                });
            });
        });
    }

    public AlertInfo(objalert: string) {
        this.ts.get('General').subscribe((general: any) => {
            this.ts.get('Info.' + objalert).subscribe((resalert: any) => {
                swal({
                    text: resalert,
                    type: 'info',
                    confirmButtonText: general.ok,
                    confirmButtonClass: 'pink darken-3 white-text '
                })
            });
        });
    }

    public SWAlertConfirm(objalert: string, confirm: any, then: any) {
        this.ts.get('SWA.' + objalert).subscribe((tsa: any) => {
            swal({
                title: tsa.title,
                input: 'text',
                showCancelButton: false,
                confirmButtonText: tsa.button,
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                confirmButtonClass: 'pink darken-3 white-text ',
                preConfirm: (input) => { return confirm(input); }
            }).then((input) => then(input));
        });
    }

    public SWError(error: string) {
        this.ts.get('General').subscribe((general: any) => {
            this.ts.get('Errors.' + error).subscribe((err: any) => {
                swal({
                    text: err,
                    type: 'error',
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: general.ok,
                    confirmButtonClass: 'pink darken-3 white-text '
                });
            });
        });
    }

    public SWErrorResponse(res: any) {
        let errortext = '';
        for (let a = 0; a < res.errors.length; a++) {
            for (let e in res.errors[a]) {
                for (let i = 0; i < res.errors[a][e].length; i++) {
                    this.ts.get('Errors.' + e + res.errors[a][e][i]).subscribe((error: string) => {
                        console.log(error);
                        errortext += error + '<br>';
                    });
                }
            }
        }

        this.ts.get('General').subscribe((general: any) => {
            swal({
                html: errortext,
                type: 'error',
                confirmButtonText: general.ok,
                confirmButtonClass: 'pink darken-3 white-text ',
            });
        });
    }

    public SWErrorLink(error: string, route: any) {
        this.ts.get('General').subscribe((general: any) => {
            this.ts.get('Errors.' + error).subscribe((err: any) => {
                swal({
                    text: err,
                    type: 'error',
                    confirmButtonText: general.ok,
                    allowOutsideClick: false,
                    confirmButtonClass: 'pink darken-3 white-text ',
                    allowEscapeKey: false
                }).then(() => {
                    this.router.navigate(route);
                });
            });
        });
    }

    public ServerError() {
        swal({
            text: 'Server error',
            type: 'error',
            allowOutsideClick: false,
            allowEscapeKey: false,
            confirmButtonClass: '  pink darken-3 white-text ',
            confirmButtonText: 'Ok'
        });
    }
}
