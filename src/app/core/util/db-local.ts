import * as moment from 'moment';

export class DBLocal {

    // var ----------------------------------------------------------------------------------------
    public persistence: boolean;
    
    public expire_in: number;

    public last_result: Table;

    // internal var -------------------------------------------------------------------------------
    private storage: Storage;

    private static CLOCK: boolean;

    private static KEY: string;
    private static VALUE: any;

    constructor(persistence: boolean) {
        this.persistence = persistence;
        this.expire_in = 15;
        
        if (this.persistence) {
            this.storage = localStorage;
        }
        else {
            this.storage = sessionStorage;
        }
    }

    public start() {
        setInterval(() => {

        }, 1000 * 60 * 60 * 2);
        // ml * s * m * h
    }

    private blank(): boolean {
        let exists = true;
        if (this.storage.getItem(DBLocal.KEY) == null ||
            this.storage.getItem(DBLocal.KEY) == 'null' ||
            this.storage.getItem(DBLocal.KEY) == '' ||
            this.storage.getItem(DBLocal.KEY) == undefined ||
            this.storage.getItem(DBLocal.KEY) == 'undefined' ||
            typeof(this.storage.getItem(DBLocal.KEY)) == 'undefined') {
            exists = false;
        }

        return exists;
    }

    // primitive ----------------------------------------------------------------------------------
    public SetValue(key: string, value: string) {
        DBLocal.KEY = key;
        DBLocal.VALUE = value;
        this.storage.setItem(DBLocal.KEY, DBLocal.VALUE);
    }

    public GetValue(key: string) {
        DBLocal.KEY = key;
        return this.storage.getItem(DBLocal.KEY);
    }

    public GetData(data_base: string) {
        DBLocal.KEY = data_base;
        if (this.blank()) {
            return JSON.parse(this.storage.getItem(DBLocal.KEY));
        }
        else {
            return null;
        }
    }

    public SetData(data_base: string, data: any) {
        DBLocal.KEY = data_base;
        DBLocal.VALUE = data;

        this.storage.setItem(DBLocal.KEY, JSON.stringify(DBLocal.VALUE));
    }

    public testing() {
        this.storage.clear();
    }

    public SelectQuery(table_name: string, columns: Array < string > , conditions?: any, offset?: number, limit?: number)
    {
        let current_col = 0; // columna actual en la consulta
        let current_real_row = 0; // row total en la consulta
        let current_row = 0; // row actual en la consulta
        let count_cell = 0; // total celdas en la consulta
        let count_real_cell = 0; // total celdas
        let count_record = 0; // total de rows
        let last_record_key = '';
        
        let regexp = new RegExp('(' + table_name + ')[\\:]{1}[0-9]{1,2}[\\:]{1}(' + columns.join("|") + ')', 'g');
        
        let key: string;
        let value: string;
        
        this.last_result = {
            'name': table_name,
            'rows': [],
            'expire': this.storage.getItem(table_name)
        };
        
        let expire = moment(this.last_result.expire);
        
        if (expire.isBefore(moment().format()))
        {
            // remove
            throw "ERROR: la tabla ha expirado."
        }
        
        let row: Row = {
            'code': 0,
            'fields': []
        }
        
        offset = !offset ? 0 : offset;
        limit = !limit ? 0 : limit;
        limit += offset;
        
        for (key in this.storage)
        {
            if (this.storage.hasOwnProperty(key))
            {
                if (key.match(regexp) || !regexp)
                {
                    value = this.storage.getItem(key);
                    
                    if (current_real_row >= offset && (current_real_row < limit || limit === offset))
                    {
                        if (row.code === 0)
                        {
                            
                            row = 
                            {
                                'code': parseInt(key.split(':')[1]),
                                'fields': 
                                [
                                    {
                                        'name': key.split(':')[2],
                                        'value': value
                                    }
                                ],
                            };
                        }
                        else
                        {
                            
                            row.fields.push(
                                {
                                    'name': key.split(':')[2],
                                    'value': value
                                }
                            );
                            
                        }
                        if (current_col === columns.length - 1)
                        {
                            current_row++;
                            if (typeof conditions != 'undefined')
                            {
                                for (let key_conditions in conditions)
                                {
                                    for (let a = 0; a < row.fields.length; a++)
                                    {
                                        if (row.fields[a].name == key_conditions && row.fields[a].value == conditions[key_conditions])
                                        {
                                            this.last_result.rows.push(JSON.parse(JSON.stringify(row)));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                this.last_result.rows.push(JSON.parse(JSON.stringify(row)));
                            }
                        }
                    }
                    count_cell++;
                    current_col++;
                    if (current_col > columns.length - 1)
                    {
                        current_col = 0;
                        current_real_row++;
                    }
                }
                if (last_record_key !== (key.split(':')[0]+key.split(':')[1]))
                {
                    count_record++;
                }
                last_record_key = key.split(':')[0]+key.split(':')[1];
                
            }
            count_real_cell++;
        }

        return this.last_result;
    }

    public InsertQuery(table_name: string, columns: Array < string >, values: Array < any >)
    {
        if (values.length !== columns.length)
        {
            throw "ERROR: los valores no corresponden a las columnas";
        }
        
        let key_code = this.NextCode(table_name);
        
        let expire = moment().add(this.expire_in, 'm');
        
        this.storage.setItem(table_name, expire.format());
        
        for (let a = 0; a < columns.length; a++)
        {
            this.storage.setItem(table_name + ':' + key_code + ':' + columns[a], values[a]);
        }
    }
    
    public UpdateQuery(table_name: string, key_code: number, columns: Array < string >, values: Array < any >)
    {
        if (values.length !== columns.length)
        {
            throw "ERROR: los valores no corresponden a las columnas";
        }
        
        this.storage.setItem(table_name, moment().add(this.expire_in, 'm').format());
        
        for (let a = 0; a < columns.length; a++)
        {
            this.storage.setItem(table_name + ':' + key_code + ':' + columns[a], values[a]);
        }
    }
    
    // public DeleteQuery(table_name: string, conditions?: any)
    // {
    //     let regexp: any;
        
    //     if (typeof conditions != 'undefined'){
    //         let regexp = new RegExp('(' + table_name + ')[\\:]{1}[0-9]{1,2}[\\:]{1}', 'g');
    //     } else {
    //         let regexp = new RegExp('(' + table_name + ')', 'g');
    //     }
        
    //     for (let key in this.storage)
    //     {
    //         if (this.storage.hasOwnProperty(key))
    //         {
    //             if (key.match(regexp) || !regexp)
    //             {
    //                 if (typeof conditions != 'undefined')
    //                 {
    //                     this.storage.removeItem(key);
    //                 }
    //                 else
    //                 {
    //                     for (let key_conditions in conditions)
    //                     {
    //                         if (key_conditions == key.split(':')[1])
    //                         {
    //                             // en process
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }
    
    // se debe mejorar
    public NextCode(table_name)
    {
        let regexp = new RegExp('(' + table_name + ')[\\:]{1}[0-9]{1,2}', 'g');
        let key_code = 1;
        
        for (let key in this.storage)
        {
            if (this.storage.hasOwnProperty(key))
            {
                if (key.match(regexp) || (!regexp && typeof key === 'string'))
                {
                    key_code = parseInt(key.split(':')[1]) + 1;
                }
            }
        }
        
        return key_code;
    }


    // delete data --------------------------------------------------------------------------------
    public DestroyData(data_base: string)
    {
        this.storage.removeItem(data_base);
    }

    public Clear()
    {
        this.storage.clear();
    }

    public ClearAll()
    {
        localStorage.clear();
        sessionStorage.clear();
    }
}

export interface Table {
    name: string;
    rows: Row[];
    expire: string;
}

export interface Row {
    code?: number;
    fields: Field[];
}

export interface Field {
    name: string;
    value: string;
}