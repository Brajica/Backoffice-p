import {Headers, Response} from '@angular/http';

import * as moment from 'moment';

import {Observable} from 'rxjs/Observable';

export class CustomRequests {
    public static CreateAuthorizationHeader(header: Headers) {
        const token = localStorage.getItem('csrf_token');
        header.append('X-CSRF-TOKEN', token);
    }

    public static ResponseData(res: Response) {
        const body = res.json();
        return body;
    }

    public static handleError(error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        try {
            if (error instanceof Response) {
                const body = error.json() || '';
                const err = body.error || JSON.stringify(body);
                errMsg = error.status + "-" + (error.statusText || '') + err;
            } else {
                errMsg = error.message ? error.message : error.toString();
            }
        } catch (e) {
            errMsg = "";
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}

export class LocalResponse {

    public OFFSET_TIME: string;

    constructor(private key: string,
                private offset_time?: string){

    }

    public subscribe(response_f?: any) {

    }
}
