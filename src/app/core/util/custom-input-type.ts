export class CustomInputType {
    public Name(event) {
        let response = true;

        const key = event.keyCode || event.which;
        const tecla = String.fromCharCode(key);
        const val = tecla.match(new RegExp('[a-zA-Z \t\b]', 'g'));

        if (!(val != null && val[0] === tecla)) {
            response = false;
        }

        return response;
    }

    public userName(event) {
        let response = true;

        const key = event.keyCode || event.which;
        const tecla = String.fromCharCode(key);
        const val = tecla.match(new RegExp('[a-z0-9\-\t\b]', 'g'));

        if (!(val != null && val[0] === tecla)) {
            response = false;
        }

        return response;
    }

    public IdNumber(event) {
        let response = true;

        const key = event.keyCode || event.which;
        const tecla = String.fromCharCode(key);
        const val = tecla.match(new RegExp('[a-zA-Z0-9\-\t\b]', 'g'));

        if (!(val != null && val[0] === tecla)) {
            response = false;
        }

        return response;
    }
}
