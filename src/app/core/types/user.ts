export interface User {

    // user data
    address?: string;
    afiliate_code?: string;
    afiliate_date?: CustomDate;
    afiliate_due_date?: CustomDate;
    avatar?: string;
    birth_date?: string;
    captcha?: string;
    code?: string;
    city?: any;
    country?: any;
    datelimit?: string;
    document_type?: string;
    document_number?: string;
    email?: string;
    firstname?: string;
    lastname?: string;
    password?: string;
    phone?: string;
    plan?: number;
    range?: number;
    sponsor?: string;
    state?: number;
    upgrade_gold?: CustomDate;
    upgrade_platinum?: CustomDate;
    username?: string;
    skype?: string;
    whatsapp?: string;
    
    // user data confirm
    confirm_email?: string;
    confirm_password?: string;
    
    level?: number;
}

export interface CustomDate {
    year: number;
    month: number;
    day: number;
}