export class Session {

    /* definicion de variables ---------------------------------------------- */
    static username : string;
    static email : string;
    static firstname : string;
    static lastname : string;
    static avatar : string;
    static range : number;
    static range_description : string;
    static level : number;
    static city : string;
    static date : string;
    static reload : boolean;

    // nunca dejar llena
    static password : string;

    static captcha : string;
    static address : string;
    static afiliate_code : number;
    static sales_code: string;
    static afiliate_date : string;
    static datelimit : string;
    static phone : number;
    static country : string;
    static whatsapp : number;
    static skype : string;
    static pack: string;
    static leg: any;
    /* end ------------------------------------------------------------------ */

    /* definicion de setters y getters -------------------------------------- */
    // importante declararlos siempre
    get username() {
        return Session.username;
    }
    set username(username) {
        Session.username = username
    }

    get email() {
        return Session.email;
    }
    set email(email) {
        Session.email = email
    }

    get firstname() {
        return Session.firstname;
    }
    set firstname(firstname) {
        Session.firstname = firstname
    }

    get lastname() {
        return Session.lastname;
    }
    set lastname(lastname) {
        Session.lastname = lastname
    }

    get avatar() {
        return Session.avatar;
    }
    set avatar(avatar) {
        Session.avatar = avatar
    }
    get pack() {
      return Session.pack;
    }
    set pack(pack: string){
      Session.pack = pack;
    }
    get range() {
        return Session.range
    }
    set range(range : number) {
        Session.range = range;
        switch (range) {
            case 1:
            Session.range_description = 'Madera';
            break;
            case 2:
            Session.range_description = 'Bronce';
            break;
            case 3:
            Session.range_description = 'Plata';
            break;
            case 4:
            Session.range_description = 'Oro';
            break;
            case 5:
            Session.range_description = 'Platino';
            break;
            case 6:
            Session.range_description = 'Diamante';
            break;
            default:
            Session.range_description = 'Madera';
            break;
        }
    }

    get range_description() {
        return Session.range_description;
    }
    set range_description(range_description : string) {
        Session.range_description = range_description;
        switch (range_description) {
            case 'Madera':
            Session.range = 1;
            break;
            case 'Bronce':
            Session.range = 2;
            break;
            case 'Plata':
            Session.range = 3;
            break;
            case 'Oro':
            Session.range = 4;
            break;
            case 'Platino':
            Session.range = 5;
            break;
            case 'Diamante':
            Session.range = 6;
            break;
            default:
            Session.range = 0;
            break;
        }
    }

    get level() {
        return Session.level
    }
    set level(level : number) {
        Session.level = level
    }

    get reload() {
        return Session.reload
    }
    set reload(reload: boolean) {
        Session.reload = reload
    }

    get password() {
        return Session.password;
    }
    set password(password) {
        Session.password = password
    }

    get captcha() {
        return Session.captcha;
    }
    set captcha(captcha) {
        Session.captcha = captcha
    }

    get address() {
        return Session.address;
    }
    set address(address) {
        Session.address = address
    }

    get afiliate_code() {
        return Session.afiliate_code;
    }
    set afiliate_code(afiliate_code) {
        Session.afiliate_code = afiliate_code
    }
    
    get sales_code() {
        return Session.sales_code;
    }
    set sales_code(sales_code) {
        Session.sales_code = sales_code;
    }

    get afiliate_date() {
        return Session.afiliate_date;
    }
    set afiliate_date(afiliate_date) {
        Session.afiliate_date = afiliate_date
    }

    get datelimit() {
        return Session.datelimit;
    }
    set datelimit(datelimit) {
        Session.datelimit = datelimit
    }

    get phone() {
        return Session.phone;
    }
    set phone(phone) {
        Session.phone = phone
    }

    get country() {
        return Session.country;
    }
    set country(country) {
        Session.country = country
    }

    get whatsapp() {
        return Session.whatsapp;
    }
    set whatsapp(whatsapp) {
        Session.whatsapp = whatsapp
    }

    get skype() {
        return Session.skype;
    }
    set skype(skype) {
        Session.skype = skype
    }

    get city() {
        return Session.city;
    }
    set city(city) {
        Session.city = city
    }

    get date() {
        return Session.date;
    }
    set date(date) {
        Session.date = date;
    }
    get leg() {
        return Session.leg;
    }
    set leg(leg) {
        Session.leg = leg;
    }
    public static sendDataLogin() {
        return {'username': Session.username, 'password': Session.password, 'captcha': Session.captcha}
    }

    public static clearSegurity() {
        Session.captcha = '';
        Session.password = '';
    }
}
