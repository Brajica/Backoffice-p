export class Commissions {
    
    public negocios: number;
    public ventas_directas: number;
    public mensual: number;
    public bono_negocio: number;
    public liderazgo: number;
    public equilibrio: number;
    public total: number;
    
    public Initialize(): Commissions {
        this.ventas_directas = 0;
        this.mensual = 0;
        this.negocios = 0;
        this.equilibrio = 0;
        this.bono_negocio = 0;
        this.liderazgo = 0;
        this.total = 0;
        return this; 
    }
}