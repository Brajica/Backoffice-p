export class Areto {
    public card_number: string;
    public expiration_date: string;
    public cvc_code: string;
    public titular: string;
    public postal_code: string;
    public package_name: string;
    public city: string;
    public street: string;
    public data: any;

    public Initialize(): Areto {
        this.card_number = '';
        this.expiration_date = '';
        this.cvc_code = '';
        this.titular = '';
        this.postal_code = '';
        this.package_name = '';
        this.data = {};
        this.street = '';
        this.city = '';
        return this;
    }
}
