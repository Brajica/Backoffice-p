import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../core/types/user';

@Component({
    selector: 'app-form-complete-register-step-four',
    templateUrl: './step-four.html'
})
export class FormCompleteRegisterStepFourComponent implements OnInit {

    @Output() step = new EventEmitter();

    @Input() user: User;

    public country_name: string;

    public check1 = false;
    public check2 = false;

    constructor() {
        // reservado
    }

    ngOnInit() {
    }

    public send() {
        this.step.emit({'action': 'send'});
    }

    public back() {
        this.step.emit({'action': 'back', 'numinterval': 1});
    }
}
