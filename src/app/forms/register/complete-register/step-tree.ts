import {Component, EventEmitter, Output} from '@angular/core';

@Component({
    selector: 'app-form-complete-register-step-tree',
    templateUrl: './step-tree.html',
    styleUrls: ['../../../../assets/css/materialize/materialize-shadows.css']
})
export class FormCompleteRegisterStepTreeComponent {

    @Output() step = new EventEmitter();

    constructor() {
        // reservado
    }

    public select(num: number) {
        this.step.emit({'action': 'selectpack', 'pack': num});
    }

    public ant() {
        this.step.emit({'action': 'back', 'numinterval': 1});
    }
}
