import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {GeneralService} from '../../../core/services/general.service';

import {User} from '../../../core/types/user';
import {ArrayList} from '../../../core/util/arraylist';
import {CustomInputType} from '../../../core/util/custom-input-type';
import {DBLocal} from '../../../core/util/db-local';

@Component({
    selector: 'app-form-complete-register-step-two',
    templateUrl: './step-two.html',
    providers: [GeneralService]
})
export class FormCompleteRegisterStepTwoComponent implements OnInit {
    private storage: DBLocal;

    @Output() step = new EventEmitter();
    @Output() errors = new EventEmitter();

    @Input() user: User;

    public custom_input_type: CustomInputType;
    public model_countries = [];

    constructor(private gs: GeneralService) {
        // reservado
    }

    ngOnInit() {
        this.storage = new DBLocal(false);

        this.custom_input_type = new CustomInputType();

        this.gs.Countries().subscribe(
            response => this.getCountriesSuccess(response),
            error => this.errors.emit({'error': 'servererror'})
        );
    }

    public next() {
        if (this.user.document_type !== '' && this.user.document_type !== '0' &&
            this.user.document_number !== '' &&
            this.user.firstname !== '' &&
            this.user.lastname !== '' &&
            // this.user.birth_date.year !== 0 &&
            // this.user.birth_date.month !== 0 &&
            // this.user.birth_date.day !== 0 &&
            this.user.country !== 0) {
            this.step.emit({'action': 'next'});
        } else {
            this.errors.emit({'error': 'inputrequire'});
        }
    }

    public back() {
        this.step.emit({'action': 'back', 'numinterval': 1});
    }

    public setCountry() {
        console.log(this.user.country);
        for (let a = 0; a < this.model_countries.length; a++) {
            if (this.model_countries[a].country_master_id == this.user.country) {
                console.log(this.model_countries[a]);
                this.storage.expire_in = 60 * 24 * 365;
                this.storage.UpdateQuery(
                    'country',
                    1,
                    ['id', 'name'],
                    [
                        this.model_countries[a].country_master_id,
                        this.model_countries[a].country_master_name
                    ]);
                break;
            }
        }
    }

    private getCountriesSuccess(response: any) {
        console.log(response);
        if (response.state == 1) {
            for (let i = 0; i < response.data.length; i++) {
                if (response.data[i].type == "old_countries") {
                    this.model_countries.push(response.data[i].attributes)
                }
            }
        } else {
            this.errors.emit({'error': 'servererror'});
        }
    }

    public dateFormat(e) {
        console.log(e);
        let response = true;

        const key = e.keyCode || e.which;
        const tecla = String.fromCharCode(key);
        const val = tecla.match(new RegExp('[0-9\-\t\b]', 'g'));

        if (!(val != null && val[0] === tecla)) {
            response = false;
        }
        if (e.key !== "Backspace") {
            try {
                if(this.user.birth_date.length == 0 && parseInt(tecla) > 1){
                    this.user.birth_date += '0';
                }
                if(this.user.birth_date.length == 4 && tecla == '/'){
                    this.user.birth_date = this.user.birth_date.substr(0, 3) + '0' + this.user.birth_date.charAt(this.user.birth_date.length - 1);
                }
                if(this.user.birth_date.length == 2 || this.user.birth_date.length == 5) {
                    this.user.birth_date += '/'
                }
            } catch (e) {
                console.error(e);
            }
        }

        return response;
    }

    public dateFormatEnd(e) {
        let response = true;

        let date = this.user.birth_date.split('/');

        if(date[2].length <= 2) {
            date[2] = '19' + date[2];
        }

        if (parseInt(date[2]) < 1900 || parseInt(date[2]) > 1999) {
            this.user.birth_date = "date invalid"
        } else {
            this.user.birth_date = date[0] + "/" + date[1] + "/" + date[2];
        }

        return response;
    }
}
