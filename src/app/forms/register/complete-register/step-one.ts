import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {User} from '../../../core/types/user';
import {CustomInputType} from '../../../core/util/custom-input-type';

@Component({
    selector: 'app-form-complete-register-step-one',
    templateUrl: './step-one.html'
})
export class FormCompleteRegisterStepOneComponent implements OnInit {

    @Output() step = new EventEmitter();
    @Output() errors = new EventEmitter();

    @Input() user: User;

    public input_type = new CustomInputType();
    public level_password: number;

    public check1 = false;

    constructor() {
        // reservado
    }

    ngOnInit() {
        this.level_password = -1;

        this.CheckPass();
    }

    public next() {
        if (this.user.password === this.user.confirm_password && this.check1) {
            this.step.emit({'action': 'next'});
        } else if (!this.check1) {
            this.errors.emit({'error': 'termsinvalid'});
        } else {
            this.errors.emit({'error': 'passwordinvalid'});
        }
    }

    public CheckPass() {

        const count = [];

        count['alpha_lower'] = (this.user.password.match(new RegExp('[a-z]', 'g')) || []).length;
        count['alpha_uper'] = (this.user.password.match(new RegExp('[A-Z]', 'g')) || []).length;
        count['number'] = (this.user.password.match(new RegExp('[0-9]', 'g')) || []).length;
        count['space'] = (this.user.password.match(new RegExp('[ ]', 'g')) || []).length;
        count['special_character'] = (this.user.password.match(new RegExp('[^a-zA-Z0-9]', 'g')) || []).length;
        count['count'] = this.user.password.length;

        if ((count['count'] < 10)) {
            this.level_password = -1;
        } else if (count['count'] < 13 &&
            count['number'] >= 1 &&
            count['alpha_uper'] >= 1 &&
            count['alpha_lower'] >= 1) {
            this.level_password = 0;
        } else if (count['count'] < 16 &&
            count['number'] >= 1 &&
            count['alpha_uper'] >= 1 &&
            count['alpha_lower'] >= 1 &&
            count['special_character'] >= 1) {
            this.level_password = 1;
        } else if (count['count'] >= 19 &&
            count['number'] >= 1 &&
            count['alpha_uper'] >= 1 &&
            count['alpha_lower'] >= 1 &&
            count['special_character'] >= 1) {
            this.level_password = 2;
        }
        console.log(this.level_password);

    }
}
