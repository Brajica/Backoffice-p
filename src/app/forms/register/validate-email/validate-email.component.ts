import {Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from '../../../core/types/user';

@Component({
    selector: 'app-form-validate-email',
    templateUrl: './validate-email.component.html'
})
export class FormValidateEmailComponent {

    @Output() validate_proccess = new EventEmitter();

    @Input() input: User;
    public email = '';

    // input_sponsor_editable
    @Input() ise: boolean;
    // input_sponsor_required
    @Input() isr: boolean;
    
    @Input() iur: boolean;
    @Input() ibr: boolean;

    constructor() {
    }

    OnSubmit() {
        if (this.input.email === this.email) {
            this.validate_proccess.emit({'success': true, 'email': this.input.email, 'username': this.input.username, 'birth_date': this.input.birth_date});
        } else {
            this.validate_proccess.emit({'error': 'emailsnomatch'});
        }
    }
}
