import { Component, Inject, OnInit } from '@angular/core';
import {MD_DIALOG_DATA} from '@angular/material';
import {User} from '../../../core/types/user';
import {ProfileService} from '../../../backoffice/user/profile/profile.service';
import {SWAlert} from '../../../core/util/swalert';
import {Router, ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Session} from '../../../core/types/session';
@Component({
  selector: 'app-update-general',
  templateUrl: './update-general.component.html',
  styleUrls: ['./update-general.component.css']
})
export class UpdateGeneralComponent   {
    public alert : SWAlert;
   constructor(@Inject(MD_DIALOG_DATA) public data: any,
              private ts : TranslateService,
              private ps : ProfileService,
              private router : Router
     ) { }

  public onSubmit() {
    console.log(Session.city)
    this.ps.updateProfileData(this.data)
      .subscribe(response => this.dataGeneralSuccess(response), error => alert("error"));
      this.alert = new SWAlert(this.ts, this.router);
  }

   private dataGeneralSuccess(response: any) {
     if (response.state >= 1 && response.state <= 100) {
      this.alert.AlertInfo("actualizados");
    } else {
      console.info(response)
      this.alert.SWErrorResponse(response);
    }
   }

 }
