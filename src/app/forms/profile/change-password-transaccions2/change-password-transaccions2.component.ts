import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form-password-transaccions2',
  templateUrl: './change-password-transaccions2.component.html'
})
export class ChangePasswordTransaccions2Component implements OnInit {
  
  public oldPassword: string;
  public newPassword: string;
  public confirmPassword: string;
  @Input() display: any;
  @Output() eventSend = new EventEmitter();
  constructor() { 
    
  }

  ngOnInit() {
    
  }
    // funcion que envia los parametros al componente padre 
  public sendPassword(){
    this.eventSend.emit({'oldPassword': this.oldPassword,
                         'newPassword': this.confirmPassword
    })
  }
}
