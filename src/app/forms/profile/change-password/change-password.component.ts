import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

import {  Router, ActivatedRoute, Params } from '@angular/router';
import {ProfileService} from '../../../backoffice/user/profile/profile.service';
import {TranslateService} from '@ngx-translate/core';
import {SWAlert} from '../../../core/util/swalert';

@Component({
	selector: 'app-forms-change-password',
	templateUrl: './change-password.component.html'
})
export class ChangePasswordComponent implements OnInit {

   @Input() input: any;
   public comfirm_password: string;

   public alert: SWAlert;

   @Output() change_password = new EventEmitter();
   @Output() errors = new EventEmitter();

  constructor(private ps: ProfileService,
              private route: ActivatedRoute,
              private ts: TranslateService,
              private router: Router) { }

	ngOnInit() {
		this.comfirm_password = '';
		this.sendCode();

		this.alert = new SWAlert(this.ts, this.router);
	}

	Send(){
		if(this.input.newpassword === this.comfirm_password){
			this.change_password.emit({'action': 'send'});
		} else {
			this.errors.emit({'error': 'passwordnotmatch'});
		}
	}
   // funcion que pasa como parametro el codigo que viene en la ruta que 
//    fue enviada el correo al mometo de solicitar cambio de contraseña
	public sendCode(){
		this.route.params.subscribe((params: Params) => {
			if(typeof(params.code) != "undefined"){
				this.ps.codeUpdate(params.code)
				.subscribe(
					response => this.validateCode(response),
					error => this.alert.ServerError()
				);
			  }
    		});
	}

	public validateCode(response: any){
		if(response.state >= 1 && response.state <= 100) {
			this.alert.SWAlertSuccessLink("passwordchange",['/']);
		} else {
			this.alert.SWErrorResponse(response);
		}
	}

}
