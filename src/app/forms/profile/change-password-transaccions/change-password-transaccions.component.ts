import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DBLocal } from '../../../core/util/db-local';
@Component({
	selector: 'app-form-password-transaccions',
	templateUrl: './change-password-transaccions.component.html'
})
export class ChangePasswordTransaccionsComponent implements OnInit {
	public answer: string;
	public storage: DBLocal;
	// Array de preguntas la cuales se muestran en la vista de este mismo componente
	public question: any = [
		  {
		     value: 1,
		  	 question: "¿Nombre de tu primera mascota?"
		  },
		  {
		  	value: 2,
		  	 question: "¿Nombre de tu profesor favorito de secundaria?"
		  },
		  {
		  	value: 3,
		  	 question: "¿Ocupacion de tu abuelo paterno?"
		  },
		  {
		  	value: 4,
		  	 question: "¿Nombre de tu mejor amigo de la infancia?"
		  },
		  {
		  	value: 5,
		  	 question: "¿Tu sobrenombre en la escuela?"
		  }
		];
    public selectquestion: number;
	@Input() input: any;
	@Input() step: number;
	@Output() event_change_password = new EventEmitter();
	
	constructor() { }
		
	ngOnInit() {
		this.storage = new DBLocal(true);
	}
      //funcion que manda los parametros al componente padre
    public sendstep(){
    	this.step++
    	this.event_change_password.emit({'step': this.step,
									     'question': this.selectquestion,
									     'answer': this.answer});
    }
    
    
 }

