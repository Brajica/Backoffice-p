import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef}      from '@angular/material';
import {MD_DIALOG_DATA}              from '@angular/material';
@Component({
  selector: 'app-pay-upgrade-pack',
  templateUrl: './pay-upgrade-pack.component.html',
  styleUrls: ['./pay-upgrade-pack.component.css']
})
export class PayUpgradePackComponent implements OnInit {
   public pay_method: any;
  constructor(@Inject(MD_DIALOG_DATA) public data: any,
             public dialogRef: MdDialogRef<PayUpgradePackComponent>) { }

  ngOnInit() {
  }

}
