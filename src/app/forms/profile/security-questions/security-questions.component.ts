import { Component, OnInit } from '@angular/core';
import {SWAlert} from '../../../core/util/swalert';
import {DBLocal} from '../../../core/util/db-local';
import {TranslateService} from '@ngx-translate/core';
import {ProfileService} from '../../../backoffice/user/profile/profile.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-form-security-questions',
  templateUrl: './security-questions.component.html'
})
export class SecurityQuestionsComponent implements OnInit {

  public storage: DBLocal;
  public alert: SWAlert;
  
  constructor(private ps: ProfileService,
	            private ts: TranslateService,
	            private router: Router) { }

  ngOnInit() {
    this.storage = new DBLocal(true);
    this.alert = new SWAlert(this.ts, this.router);
  }

}
