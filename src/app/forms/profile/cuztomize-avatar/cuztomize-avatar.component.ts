import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ProfileService } from "../../../backoffice/user/profile/profile.service";
import { TranslateService } from "@ngx-translate/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { SWAlert } from "../../../core/util/swalert";
import { DBLocal } from "../../../core/util/db-local";
import { Session } from "../../../core/types/session";
import { FileUploader } from "ng2-file-upload";
import { GLOBALS } from "../../../core/config/config";

@Component({
  selector: "app-form-cuztomize-avatar",
  templateUrl: "./cuztomize-avatar.component.html"
})
export class CuztomizeAvatarComponent implements OnInit {
  private index: number;
  public avatars: any;
  public model_avatar: number;
  public alert: SWAlert;
  public uploader: FileUploader = new FileUploader({
    url: GLOBALS.URL_BASE + "user/upload/avatar"
  });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  public storage: DBLocal;
  constructor(
    private ps: ProfileService,
    private route: ActivatedRoute,
    private ts: TranslateService,
    private router: Router
  ) {}

  ngOnInit() {
    this.storage = new DBLocal(true);

    this.index = 5;
    this.avatars = [1, 2, 3, 4, 5];
    this.alert = new SWAlert(this.ts, this.router);
  }

  public VerMas() {
    const index = this.index;
    if (index <= 36) {
      for (let i = index; i < index + 10 && i < 36; i++) {
        this.avatars.push(this.index + 1);
        this.index++;
      }
    }
  }

  public sendImg() {
    this.ps
      .UpdateAvatar(Session.username, this.model_avatar)
      .subscribe(
        response => this.responseAvatar(response),
        error => this.alert.ServerError()
      );
  }
  public responseAvatar(response: any) {
    if (response.state >= 1 && response.state <= 100) {
      Session.avatar = response.data.attributes.rut_img;
      this.alert.AlertInfo("avatarupdate");
    } else {
      this.alert.SWErrorResponse(response);
    }
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  public SendImgProfile(event) {
     var files = event.target.files || event.srcElement.files;
      var file = files[0];
      this.ps.ImgProfile({ username: Session.username, file: file }).subscribe(
          response => this.imgprofile(response),
          error => this.alert.ServerError()
        );
  }

  private imgprofile(data: any) {
    this.storage.UpdateQuery('session' , 1, ['avatar'], [data.data.attributes.rut_img])
    Session.avatar = data.data.attributes.rut_img
  }
}
