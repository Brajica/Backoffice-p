import { Component, OnInit, Inject } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {MD_DIALOG_DATA} from '@angular/material';
import {Session} from '../../../core/types/session';
import { Router } from '@angular/router';
import { PayUpgradePackComponent } from '../pay-upgrade-pack/pay-upgrade-pack.component';

@Component({
  selector: 'app-upgrade-package',
  templateUrl: './upgrade-package.component.html',
  styleUrls: ['./upgrade-package.component.css']
})
export class UpgradePackageComponent implements OnInit  {
  public user: Session;
  public up_pack: any;
  public pay_method: any;
  public step : number = 1;
  constructor(@Inject(MD_DIALOG_DATA) public data: any,
              public dialogRef: MdDialogRef<UpgradePackageComponent>,
              public Dialogo: MdDialog,
              private router: Router) { }

    ngOnInit(){
      this.user = new Session();
    }
    // funcion para proceder a los metodos de pago para poder actualizar
    // el paquete
  public next(){
    if(this.up_pack === '' || this.up_pack === undefined){
      alert('Elige el paquete a actualizar');
    }else{
      console.info(this.step)
    this.step++;  
    console.info(this.step)
    }
  }
  public back(){
    this.step--;
  }
  public paymethod(){
    if(this.pay_method === '' || this.pay_method === undefined) {
      alert("elige el metodo");
    }else{
     this.dialogRef.close();
     this.router.navigate(['backoffice/profile/pay/'+ this.pay_method + '/' + this.up_pack])
    }
  }
}
