import {Component, EventEmitter, Output, Input} from '@angular/core';

import {User} from '../../core/types/user';

@Component({
    selector: 'app-form-login',
    templateUrl: './login.component.html'
})
export class FormLoginComponent {

    @Output() login_event = new EventEmitter();

    @Input() user: User;

    public icon = {'user': false, 'password': false};

    constructor() {
    }

    onSubmit() {
        this.login_event.emit(true);
    }

    public activateIcon(name: string) {
        this.icon[name] = true;
    }

    public deactivateIcon(name: string) {
        this.icon[name] = false;
    }
}
