import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {User} from '../../core/types/user';

@Component({
    selector: 'app-form-recover-password',
    templateUrl: './recover-password.component.html'
})
export class FormRecoverPasswordComponent implements OnInit {

    @Output() validate_password = new EventEmitter();

    @Input() input: User;

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {
    }

    OnSubmit() {
        if (this.input.password === this.input.confirm_password) {
            this.route.params.subscribe((params: Params) => {
                this.validate_password.emit({'password': this.input.confirm_password, 'code': params.code});
            });
        } else {
            this.validate_password.emit({'error': 'emailsnomatch'});
        }
    }

}
