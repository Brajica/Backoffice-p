import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {RecaptchaModule} from 'ng-recaptcha';
import {RecaptchaFormsModule} from 'ng-recaptcha/forms';
import {FileSelectDirective} from 'ng2-file-upload';

import {FormLoginComponent} from './login/login.component';
import {FormEpinComponent} from './paymethod/epin/epin.component';
import {FormValidateEmailComponent} from './register/validate-email/validate-email.component';
import {FormCompleteRegisterStepOneComponent} from './register/complete-register/step-one';
import {FormCompleteRegisterStepTwoComponent} from './register/complete-register/step-two';
import {FormCompleteRegisterStepTreeComponent} from './register/complete-register/step-tree';
import {FormCompleteRegisterStepFourComponent} from './register/complete-register/step-four';
import {FormRecoverPasswordComponent} from './recover-password/recover-password.component';
import {ChangePasswordComponent} from './profile/change-password/change-password.component';
import {ChangePasswordTransaccionsComponent} from './profile/change-password-transaccions/change-password-transaccions.component';
import {ChangePasswordTransaccions2Component} from './profile/change-password-transaccions2/change-password-transaccions2.component';
import {AretoComponent} from './paymethod/areto/areto.component';
import {CuztomizeAvatarComponent} from './profile/cuztomize-avatar/cuztomize-avatar.component';
import {SecurityQuestionsComponent} from './profile/security-questions/security-questions.component';
import { UpdateGeneralComponent } from './profile/update-general/update-general.component';
import { UpgradePackageComponent } from './profile/upgrade-package/upgrade-package.component';
import { PayUpgradePackComponent } from './profile/pay-upgrade-pack/pay-upgrade-pack.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MaterialModule,
        RecaptchaModule,
        RecaptchaFormsModule,
        TranslateModule
    ],
    declarations: [
        FormLoginComponent,
        FormValidateEmailComponent,
        FormEpinComponent,
        FormCompleteRegisterStepOneComponent,
        FormCompleteRegisterStepTwoComponent,
        FormCompleteRegisterStepTreeComponent,
        FormCompleteRegisterStepFourComponent,
        FormRecoverPasswordComponent,
        ChangePasswordComponent,
        ChangePasswordTransaccionsComponent,
        ChangePasswordTransaccions2Component,
        AretoComponent,
        CuztomizeAvatarComponent,
        SecurityQuestionsComponent,
        FileSelectDirective,
        UpdateGeneralComponent,
        UpgradePackageComponent,
        PayUpgradePackComponent
    ],
    exports: [
        FormLoginComponent,
        FormValidateEmailComponent,
        FormEpinComponent,
        FormCompleteRegisterStepOneComponent,
        FormCompleteRegisterStepTwoComponent,
        FormCompleteRegisterStepTreeComponent,
        FormCompleteRegisterStepFourComponent,
        FormRecoverPasswordComponent,
        ChangePasswordComponent,
        ChangePasswordTransaccionsComponent,
        ChangePasswordTransaccions2Component,
        AretoComponent,
        CuztomizeAvatarComponent,
        SecurityQuestionsComponent,
        UpdateGeneralComponent,
        UpgradePackageComponent,
        PayUpgradePackComponent
    ],
    entryComponents: [
        PayUpgradePackComponent
    ]
})
export class CustomFormsModule {
}
