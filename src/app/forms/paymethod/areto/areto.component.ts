import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Areto} from '../../../core/types/areto';

@Component({
    selector: 'app-form-areto',
    templateUrl: './areto.component.html',
    styleUrls: ['./areto.component.css']
})
export class AretoComponent implements OnInit {

    @Input() areto: Areto;
    @Output() areto_emit = new EventEmitter();
    public isValidDate = true;
    public isValidNumber = true;
    public isValidCvc = true;
    public isValidName = true;
    public isValidPostal = true;
    public iAgree = true;
    public checked: boolean;

    constructor() {
    }

    ngOnInit() {
    }

    //Controla el formato del campo card number
    public formatCardNumber(event: any) {
        event = (event) ? event : window.event;
        let charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }

        if (typeof (this.areto.card_number + event.ket) != 'undefined') {

            if (typeof(this.areto.card_number) == 'undefined' || this.areto.card_number.length == 0) {
                this.areto.card_number = event.key;
                event.preventDefault();
            } else {
                this.areto.card_number += event.key;
                event.preventDefault();
            }

            if (this.areto.card_number.length > 6) {
                this.areto.card_number = this.areto.card_number.substr(0, 19);
                event.preventDefault();
            }

            let card_number = (this.areto.card_number).split(" ").join("").match(/[0-9]{0,4}/g);

            let format = '';
            for (let i = 0; i < (card_number.length - 1); i++) {

                if (card_number[i].length == 4 && (i + 1) < card_number.length - 1) {
                    format += card_number[i] + ' ';
                } else {
                    format += card_number[i];
                }

            }

            this.areto.card_number = format
            event.preventDefault();
        }
    }


    //Controla el formato del campo fecha de expiración de la TC
    public formatCardDate(event: any) {
        event = (event) ? event : window.event;
        let charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }

        let current_date = new Date();


        if (typeof (this.areto.expiration_date + event.key) != 'undefined') {

            if (typeof(this.areto.expiration_date) == 'undefined' || this.areto.expiration_date.length == 0) {
                this.areto.expiration_date = event.key;
                event.preventDefault();
            } else {
                this.areto.expiration_date += event.key;
                event.preventDefault();
            }

            if (this.areto.expiration_date.length > 6) {
                this.areto.expiration_date = this.areto.expiration_date.substr(0, 7);
                event.preventDefault();
            }

            //console.log((this.areto.expiration_date+event.key).length);
            if ((this.areto.expiration_date).length == 1 && parseInt(event.key) > 1) {
                this.areto.expiration_date = 0 + (event.key) + "/";
                event.preventDefault();
            }

            if (this.areto.expiration_date.length == 2 && parseInt(this.areto.expiration_date) > 12) {
                this.areto.expiration_date = "01/" + event.key;
            } else if (this.areto.expiration_date.length == 2 && parseInt(this.areto.expiration_date) <= 12) {
                this.areto.expiration_date += "/";
            }

            if (this.areto.expiration_date.length == 7) {
                console.log('success');
                let card_date = (this.areto.expiration_date).split("/");
                this.isValidDate = parseInt(card_date[1]) < current_date.getFullYear() ? false : true;
            }

        }

    }

    public formatCardCvc(event: any) {
        event = (event) ? event : window.event;
        let charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }

        if (typeof (this.areto.cvc_code + event.key) != 'undefined') {
            if (typeof(this.areto.cvc_code) == 'undefined' || this.areto.cvc_code.length == 0) {
                this.areto.cvc_code = event.key;
                event.preventDefault();
            } else {
                this.areto.cvc_code += event.key;
                event.preventDefault();
            }

            if (this.areto.cvc_code.length > 4) {
                this.areto.cvc_code = this.areto.cvc_code.substr(0, 4);
                event.preventDefault();
            }
        }
    }

    private formVerification(){
        let is_valid = true;
        let regExpCard = /^((67\d{2})|(4\d{3})|(5[1-5]\d{2})|(6011))(-?\s?\d{4}){3}|(3[4,7])\ d{2}-?\s?\d{6}-?\s?\d{5}$/;
        if(!regExpCard.test(this.areto.card_number)){
            is_valid = false;
            this.isValidNumber = false;
        }
        if(this.areto.expiration_date.length < 7){
            is_valid = false;
            this.isValidDate = false;
        }
        if(this.areto.cvc_code.length < 3){
            is_valid = false;
            this.isValidCvc = false;
        }
        if(this.areto.titular.length <= 1){
            is_valid = false;
            this.isValidName = false;
        }
        if(this.areto.postal_code.length <= 1){
            is_valid = false;
            this.isValidPostal = false;
        }
        if(this.areto.city.length <= 1){
          is_valid = false;
        }
        if(this.areto.street.length <= 1){
          is_valid = false;
        }
        if(this.checked == false){
            is_valid = false;
            this.iAgree = false;
        }

        return is_valid;
    }

    public emitInputs() {
        if(this.formVerification()){
            this.areto_emit.emit({
                'expirate_date': this.areto.expiration_date,
                'card_number': this.areto.card_number,
                'cvc_code': this.areto.cvc_code,
                'titular': this.areto.titular,
                'postal_code': this.areto.postal_code
            })
        }
    }

}
