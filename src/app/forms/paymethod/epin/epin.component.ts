import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
@Component({
    selector: 'app-form-epin',
    templateUrl: './epin.component.html'
})
export class FormEpinComponent implements OnInit {

    @Output() epin_event = new EventEmitter();

    public epin: any;

    constructor(private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.epin = {'key': '', 'code': '', 'captcha':'', 'username': ''};
    }

    public Send() {
        // console.log('send');
        this.route.params.subscribe(params => {
          this.epin.username = params.user
        })

        this.epin_event.emit(this.epin);
    }
}
