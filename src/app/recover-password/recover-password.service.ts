import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {CustomRequests} from '../core/util/services';
import {GLOBALS} from '../core/config/config';

@Injectable()
export class RecoverPasswordService {

  constructor(private http: Http) { }

    public validateEmail(inputs: any) {
        return this.http.post(GLOBALS.URL_BASE + 'user/recover/password/outside', inputs).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
    }

     public validatePassword(password: string, code: string){
         return this.http.post(GLOBALS.URL_BASE + 'user/recoverpassword',{'newpassword': password, 'code': code}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }

    //  /user/recover/password/outside


    public validateEmail2(email: string, newpassword: string, oldpassword: string) {
        const headers = new Headers();

        CustomRequests.CreateAuthorizationHeader(headers);

        const options = new RequestOptions({headers: headers});

        return this.http.post(GLOBALS.URL_BASE + 'user/recoverpassword',{'email': email, 'newpassword': newpassword, 'oldpassword': oldpassword}, options).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
    }

}
