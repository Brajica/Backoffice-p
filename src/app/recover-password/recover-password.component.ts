import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {RecoverPasswordService} from './recover-password.service';
import {SessionService} from '../core/services/session.service';
import {TranslateService} from '@ngx-translate/core';

import {GLOBALS} from '../core/config/config';

import {SWAlert} from '../core/util/swalert';
import {User} from '../core/types/user';

declare var particlesJS: any;

@Component({
    selector: 'app-recover-password',
    templateUrl: './recover-password.component.html',
    providers: [SessionService, RecoverPasswordService],
    styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent implements OnInit {

    private alert: SWAlert;

    public code: string;

    public user: User;

    constructor(private ts: TranslateService,
                private ss: SessionService,
                private rps: RecoverPasswordService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {

        // this.ss.LoginCheck();

        particlesJS.load('content-particle', GLOBALS.URL_BASE_FONTEND + 'assets/config/particlesjs-config.json');

        if (localStorage.getItem('lang') == null) {
            localStorage.setItem('lang', 'en');
        }
        this.ts.use(localStorage.getItem('lang'));

        this.alert = new SWAlert(this.ts, this.router);

        this.user = {};
        this.user.username = '';
        this.user.birth_date = '';
        this.user.email = '';
        this.user.confirm_email = '';
        this.user.password = '';
        this.user.confirm_password = '';
        this.user.captcha = '';


        this.route.params.subscribe(params => {
            if (params['code'] != null) {
                this.code = params['code'];
            }
        });

        setTimeout(() => {
            this.onResize();
        }, 1000);
        window.addEventListener('resize', () => {
            this.onResize();
        });
    }

    public onResize() {
        const window_h = window.innerHeight;
        const element_h = document.getElementById('content-form').offsetHeight;
        document.getElementById('content-form').style.marginTop = Math.max(0, (window_h / 2) - (element_h / 2) - 50) + 'px';
    }

    public sendValidate(event: any): void {
        if (typeof (event.error) === 'undefined') {
            this.rps.validateEmail(event)
                .subscribe(
                    response => this.ValidateEmailSuccess(response),
                    error => this.alert.ServerError()
                );
        } else {
            this.alert.SWError(event.error);
        }
    }

    public sendNewPassword(event: any): void {
        if (typeof (event.error) === 'undefined') {
            this.rps.validatePassword(event.password, event.code)
                .subscribe(
                    response => this.validatePassword(response),
                    error => this.alert.ServerError()
                );
        } else {
            this.alert.SWError(event.error);
        }
    }

    public validatePassword(response: any) {
        if (response.state >= 1 && response.state <= 100) {
            this.alert.SWAlertSuccessLink('emailvalidaterecoverypassword', ['/login']);
        } else {
            this.alert.SWErrorResponse(response);
        }
    }

    public ValidateEmailSuccess(response: any) {
        if (response.state >= 1 && response.state <= 100) {
            this.alert.SWAlertSuccessLink('emailvalidaterecoveryemail', ['/login']);
        } else {
            this.alert.SWErrorResponse(response);
        }
    }

    public langChange(lang: string) {
        localStorage.setItem('lang', lang);
        this.ts.use(lang);
    }
}
