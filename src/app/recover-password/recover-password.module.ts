import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {CustomFormsModule} from '../forms/forms.module';

import {RecoverPasswordComponent} from './recover-password.component';

export const routes: Routes = [
    {path: '', component: RecoverPasswordComponent},
    {path: ':code', component: RecoverPasswordComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RecoverPasswordRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        CustomFormsModule,
        RecoverPasswordRoutingModule,
        TranslateModule
    ],
    declarations: [
        RecoverPasswordComponent
    ]
})
export class RecoverPasswordModule {
}