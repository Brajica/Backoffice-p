import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {SupportComponent} from './support.component';

export const routes: Routes = [
    {
        path: '',
        component: SupportComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SupportRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        SupportRoutingModule,
        TranslateModule
    ],
    declarations: [
        SupportComponent
    ]
})
export class SupportModule {
}