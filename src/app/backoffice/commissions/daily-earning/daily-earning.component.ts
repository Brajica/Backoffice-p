import { Component, OnInit } from '@angular/core';
import { CommissionsService } from '../commissions.service';
import {SWAlert} from '../../../core/util/swalert';
import {Commissions} from '../../../core/types/commissions';
import {TranslateService} from '@ngx-translate/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
@Component({
  selector: 'app-daily-earning',
  templateUrl: './daily-earning.component.html'
})
export class DailyEarningComponent implements OnInit {
  public Comisiones: Commissions;
  public alert: SWAlert;
  constructor(private cs: CommissionsService,
	            private route: ActivatedRoute,
	            private ts: TranslateService,
              private router: Router) { }

  ngOnInit() {
    this.alert = new SWAlert(this.ts, this.router);
    this.Comisiones = new Commissions().Initialize();
  }
  
  
  public GetCommissions (){
    this.cs.Comissions({'username': 'frbs0531'})
    .subscribe(
      response => this.ShowCommissions(response),
      error => this.alert.ServerError())
  }
  
  public ShowCommissions(response: any){
    if(response.state >= 1 && response.state <= 100) {
       this.Comisiones.negocios = response.data.comisions_app_bundle;
       this.Comisiones.total = this.Comisiones.ventas_directas +
                               this.Comisiones.mensual +
                               this.Comisiones.negocios +
                               this.Comisiones.equilibrio +
                               this.Comisiones.bono_negocio +
                               this.Comisiones.liderazgo
     }else{
       this.alert.SWErrorResponse(response)
     }
  }
  

}
