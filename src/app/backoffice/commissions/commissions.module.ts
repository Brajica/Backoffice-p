import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule } from '@angular/forms';
import {MdTabsModule} from '@angular/material';
import {CommissionsComponent} from './commissions.component';
import { DailyEarningComponent } from './daily-earning/daily-earning.component';
import { CommissionHistoryComponent } from './commission-history/commission-history.component';
import { TransactionsHistoryComponent } from './transactions-history/transactions-history.component';
import { DataFilterPipe } from './commission-history/data-filter.pipe';
import { CommissionsService } from './commissions.service';
export const routes: Routes = [
    {
        path: '',
        component: CommissionsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CommissionsRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        CommissionsRoutingModule,
        TranslateModule,
        FormsModule,
        DataTableModule,
        MdTabsModule
    ],
    declarations: [
        CommissionsComponent,
        DailyEarningComponent,
        CommissionHistoryComponent,
        TransactionsHistoryComponent,
        DataFilterPipe
    ],
    providers : [
      CommissionsService    
    ]
})
export class CommissionsModule {
}