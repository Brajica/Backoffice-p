import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transactions-history',
  templateUrl: './transactions-history.component.html'
})
export class TransactionsHistoryComponent implements OnInit {
  
  public rowsOnPage = 5;
  
  constructor() { }

  ngOnInit() {
  }

}
