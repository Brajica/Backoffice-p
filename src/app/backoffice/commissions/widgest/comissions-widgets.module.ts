import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComissionsComponent } from './comissions/comissions.component';
import {TranslateModule} from '@ngx-translate/core';
import { MerlimlifeWidgetsModule } from '../../merlimlife/widgest/merlimlife-widgets.module';
@NgModule({
  imports: [
    CommonModule,
    MerlimlifeWidgetsModule,
    TranslateModule
  ],
  declarations: [
    ComissionsComponent
  ],
  exports: [
    ComissionsComponent
  ]
})
export class ComissionsWidgetsModule { }
