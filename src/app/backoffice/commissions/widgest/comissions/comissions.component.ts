import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-widget-comissions',
  templateUrl: './comissions.component.html',
  styleUrls: ['./comissions.component.css']
})
export class ComissionsComponent implements OnInit {
  public width: number = 0;
  constructor() { }

  ngOnInit() {
    this.ResizeWidth();
    window.addEventListener('resize', () => { this.ResizeWidth(); });
  }

  private ResizeWidth(){
        this.width = document.getElementById('content').offsetWidth;
    }
}
