import { Component, OnInit } from '@angular/core';
import {MdTabsModule} from '@angular/material';

 export class Json {
  paquetenegocios: number;
  bonoequilibrio: number;
  ventasdirectas: number;
  volventas: number;
  bononegocios: number;
  bonoliderazgo: number;
  total: number;
}

export const json: Json[] = [
  {  paquetenegocios: 150, bonoequilibrio: 80, ventasdirectas: 0, volventas: 0, bononegocios: 0, bonoliderazgo: 0, total: 278 },
  {  paquetenegocios: 149, bonoequilibrio: 80, ventasdirectas: 0, volventas: 0, bononegocios: 0, bonoliderazgo: 0, total: 277 },
  {  paquetenegocios: 148, bonoequilibrio: 80, ventasdirectas: 0, volventas: 0, bononegocios: 0, bonoliderazgo: 0, total: 276 },
  {  paquetenegocios: 147, bonoequilibrio: 80, ventasdirectas: 0, volventas: 0, bononegocios: 0, bonoliderazgo: 0, total: 275 },
  {  paquetenegocios: 146, bonoequilibrio: 80, ventasdirectas: 0, volventas: 0, bononegocios: 0, bonoliderazgo: 0, total: 274 },
  {  paquetenegocios: 145, bonoequilibrio: 80, ventasdirectas: 0, volventas: 0, bononegocios: 0, bonoliderazgo: 0, total: 278 }
];

@Component({
  selector: 'app-commission-history',
  templateUrl: './commission-history.component.html'
})
export class CommissionHistoryComponent implements OnInit {

    public data;
    public filterQuery = '';
    public rowsOnPage = 10;
  constructor() { }

  ngOnInit(): void {
            setTimeout(() => {
                this.data = json;
            }, 1000);
  }


}
