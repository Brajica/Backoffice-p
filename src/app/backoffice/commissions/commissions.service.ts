import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { CustomRequests } from '../../core/util/services';
import { GLOBALS } from '../../core/config/config';
@Injectable()
export class CommissionsService {

  constructor(private http: Http ) { }
  
    public Comissions(data: any){
         return this.http.post(GLOBALS.URL_BASE + 'commissions',{'username':data.username}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }

}
