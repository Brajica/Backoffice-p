import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ComissionsWidgetsModule } from '../commissions/widgest/comissions-widgets.module'
import {UserWidgetsModule} from '../user/widgets/widgets.module';

import {LandingComponent} from './landing.component';

export const routes: Routes = [
    {
        path: '',
        component: LandingComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LandingRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        UserWidgetsModule,
        LandingRoutingModule,
        TranslateModule,
        ComissionsWidgetsModule
    ],
    declarations: [
        LandingComponent
    ]
})
export class LandingModule {
}