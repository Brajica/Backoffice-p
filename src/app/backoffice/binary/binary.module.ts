import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {BinaryComponent} from './binary.component';

export const routes: Routes = [
    {
        path: '',
        component: BinaryComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BinaryRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        BinaryRoutingModule,
        TranslateModule
    ],
    declarations: [
        BinaryComponent
    ]
})
export class BinaryModule {
}