import { Component, OnInit, state } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';
import {DBLocal} from '../../../../core/util/db-local';
import {Session} from '../../../../core/types/session';
import {UpgradePackageComponent} from '../../../../forms/profile/upgrade-package/upgrade-package.component';
import {ProfileService} from '../../profile/profile.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {SWAlert} from '../../../../core/util/swalert';
@Component({
    selector: 'app-widget-profile',
    templateUrl: './widget-profile.component.html',
    styleUrls: ['./widget-profile.component.css']
})
export class WidgetProfileComponent implements OnInit {
    private storage: DBLocal;
    public window_data: any;
    public alert : SWAlert;
    public user: Session;
    public check_center: boolean = true;
    public check_left: boolean;
    public check_rigth: boolean;

    constructor(private ps: ProfileService,
                public Dialog: MdDialog,
                private route : ActivatedRoute,
                private ts : TranslateService,
                private router : Router) {

    }

        ngOnInit() {
            this.alert = new SWAlert(this.ts, this.router);
            this.storage = new DBLocal(true);
            this.window_data = {width: 0}

            this.user = new Session();
            // console.info(Session.firstname)
            this.ResizeWidth();
            window.addEventListener('resize', () => {
                this.ResizeWidth();
            });
        //    peticion realizada para traer los datos del usuario que esta en session (profile.service)
            if (this.user.firstname === '' || this.user.firstname === undefined) {
                this.ps.getDataGeneral(Session.username)
                .subscribe(response => this.dataGeneralSuccess(response),
                error => console.error(error))
        //this.alert = new SWAlert(this.ts, this.router);
        }else{
            console.info(this.user.firstname)
        }
        }
        public openDialog(){
        let dialogRef = this.Dialog.open(UpgradePackageComponent, {
            width: '40%',
            data:this.user
        })
        }
        private ResizeWidth() {
            this.window_data.width = document.getElementById('widget-profile').offsetWidth;
            // console.log(this.window_data.width);
        }
// funcion que llena los datos del usuario para la session
        public dataGeneralSuccess(response) {
        console.log(response)
        if (response.state >= 1 && response.state <= 100) {
        this.user.leg           = response.data.attributes.leg;              
        this.user.pack          = response.data.attributes.pack;
        this.user.city          = response.data.attributes.city;
        this.user.phone         = response.data.attributes.phone;
        this.user.email         = response.data.attributes.email;
        this.user.skype         = response.data.attributes.skype;
        this.user.address       = response.data.attributes.address;
        this.user.country       = response.data.attributes.country;
        this.user.lastname      = response.data.attributes.lastname;
        this.user.whatsapp      = response.data.attributes.whatsapp;
        this.user.datelimit     = response.data.attributes.datelimit;
        this.user.firstname     = response.data.attributes.firstname;
        this.user.sales_code    = response.data.attributes.sales_code;
        this.user.afiliate_code = response.data.attributes.cod_id;
        this.user.afiliate_date = response.data.attributes.dateafiliation;
        } else {
        console.info(response);
        }
    }
// funcion que se encarga de enviar datos al servicio (profile.service) para poder actaulizar
// el estado de por donde se van agregar los directos
    public UpdateDirects(){
    this.ps.updateDirects({'username': Session.username, 'side': this.user.leg})
                .subscribe(response => this.succesDirects(response),
                error => this.error())
    }
    private succesDirects(response: any){
        if(response.state >= 1 && response.state <= 100) {
            this.alert.AlertInfo("update_side_success");
        }
    }
    public error (){
       this.alert.SWError('error_update_side');
    }
    public CopyToClipboard(containerid) {
        if (window.getSelection) {
            let range = document.createRange();
            let Selection = window.getSelection();
            range.selectNode(document.getElementById(containerid));
            Selection.addRange(range);
            document.execCommand("Copy");
            Selection.removeAllRanges();
        } else {
            alert('Not supported, please use the command: crtl + c');
        }
    }
}
