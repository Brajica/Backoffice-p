import {NgModule} from '@angular/core';
import {AmChartsModule} from '@amcharts/amcharts3-angular';
import {CustomFormsModule} from '../../../forms/forms.module';
import {CommonModule} from '@angular/common';
import { MdButtonModule, MdSlideToggleModule, MaterialModule } from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {WidgetProfileComponent} from './widget-profile/widget-profile.component';
import {LatestAffiliatesComponent} from './latest-affiliates/latest-affiliates.component';
import {ProfileService} from '../profile/profile.service';
import {AffiliateRecognitionComponent} from './affiliate-recognition/affiliate-recognition.component';
import { MdDialogModule } from '@angular/material';
import {UpgradePackageComponent} from '../../../forms/profile/upgrade-package/upgrade-package.component';
import {FormsModule} from '@angular/forms';
@NgModule({
    imports: [
        CommonModule,
        MdButtonModule,
        MdSlideToggleModule,
        AmChartsModule,
        TranslateModule,
        MdDialogModule,
        CustomFormsModule,
        FormsModule,
        MaterialModule
    ],
    declarations: [
        WidgetProfileComponent,
        LatestAffiliatesComponent,
        AffiliateRecognitionComponent
    ],
    exports: [
        WidgetProfileComponent,
        LatestAffiliatesComponent,
        AffiliateRecognitionComponent
    ],
    entryComponents:[
       UpgradePackageComponent
    ],
    providers: [
        ProfileService
    ]
})
export class UserWidgetsModule {
}
