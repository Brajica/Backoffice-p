import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProfileService } from '../../profile/profile.service';
import {  AmChartsService } from "@amcharts/amcharts3-angular";
@Component({
    selector: 'app-widget-latest-affiliates',
    templateUrl: './latest-affiliates.component.html'
})
export class LatestAffiliatesComponent implements OnInit {
    public window_data: any;
    private chart: any;
    public data_afiliates: any;
    public countries: any = [];
    public ShowA: any = [];
    public con: number = 0;
    public last_afilliates: any = [];
    private index = 5;

    @Output() general_event = new EventEmitter();
    constructor(private AmCharts: AmChartsService,private ps: ProfileService) { }

    ngOnInit() {
        this.getLastAfiliates();
        this.window_data = { width :0 };
        this.ResizeWidth();
        window.addEventListener('resize', () => { this.ResizeWidth(); });
        this.getCountries();
        setInterval(() => { this.ShowAfiliates();    },3000)
    }

    public getLastAfiliates () {
        this.ps.GetAfiliates().subscribe(
            response => this.successGet(response),
            error => this.errorsGet(error)
        );
    }
    private getCountries(){
        this.ps.GetCountries()
        .subscribe(
            response => this.succesCountries(response),
            error => this.errorCountries(error)
        );
    }
    private ShowAfiliates(){
        let container = this.data_afiliates;

        this.last_afilliates.pop()
        this.last_afilliates.unshift(container[this.index]);

        this.index++;
        if(this.index >= container.length) {
            this.index = 0;
        }
    }

    private succesCountries(response: any){
        let countries = response.data.attributes;
        for (let i in countries){
            let item = {"title": countries[i].country_master_name,
            "id": countries[i].CODE,
            "selectable": false }
            this.countries.push(item)
        }
        this.chart = this.AmCharts.makeChart("map", {
            "type": "map",
            "theme": "light",
            "projection": "equirectangular",
            "dataProvider": {
                "map": "worldLow",
                "areas": this.countries
            },
            "areasSettings": {
                "color": "#384352",
                "rollOverColor": "#000000",
                "outlineColor":"#123",
                "unlistedAreasColor": "#fff"
            },
            "backgroundAlpha": 1,
            "backgroundColor": "#1485DC"
        });
        this.chart.path = "/node_modules/amcharts3/amcharts/";

    }

    private errorCountries (response: any) {
        console.error(response.errors)
    }
    private ResizeWidth() {
        this.window_data.width = document.getElementById('table-afiliates').offsetWidth;
    }

    private successGet( response: any){
        if(response.state >= 1 && response.state <= 100) {
            this.data_afiliates = response.data.attributes;
            let item;
            let attributes = response.data.attributes;
            console.info(attributes)
            for(let i = 0; i <= 2; i++){
               item = {'username': attributes[i].username, 'code': attributes[i].code,
               'description_pack': attributes[i].description_pack, 'country': attributes[i].country
              }
              this.last_afilliates.push(item)
            }
            console.info(this.last_afilliates)
        } else {
            console.error(response.data)
        }
    }
    private errorsGet(response: any){
        console.error(response.errors)
    }


}
