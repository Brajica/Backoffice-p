import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MaterialModule} from '@angular/material';
import {CustomFormsModule} from '../../../forms/forms.module'

import {ProfileService} from './profile.service';
import { UpdateGeneralComponent } from '../../../forms/profile/update-general/update-general.component';
import {ProfileComponent} from './profile.component';
import {DataGeneralComponent} from './data-general/data-general.component';
import {ContactProfileComponent} from './contact-profile/contact-profile.component';
import {CustomizeAvatarComponent} from './customize-avatar/customize-avatar.component';
import {DocumentationComponent} from './documentation/documentation.component';
import {ManagePasswordsComponent} from './manage-passwords/manage-passwords.component';
import {FormChangePasswordsComponent} from './form-change-passwords/form-change-passwords.component';
import {FormChangePasswordsTransaccionsComponent} from './form-change-passwords-transaccions/form-change-passwords-transaccions.component';
import { SecurityQuestionsComponent } from './security-questions/security-questions.component';
import { MdDialogModule } from '@angular/material';
import { EpinComponent } from '../../../register/pay-method/epin/epin.component';
export const routes: Routes = [
    {
        path: '',
        component: ProfileComponent
    },
    {
        path: 'change-password',
        component: FormChangePasswordsComponent
    },
    {
        path: 'change-password/:code',
        component: FormChangePasswordsComponent
    },
    {
       path: 'change-password-trans',
       component : FormChangePasswordsTransaccionsComponent
    },
    {
      path: 'security-question',
      component : SecurityQuestionsComponent
    },
    {
        path: 'pay/epin/:pack',
        component : EpinComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        ProfileRoutingModule,
        NgbModule,
        TranslateModule,
        FormsModule,
        CustomFormsModule,
        MaterialModule
    ],
    declarations: [
        ProfileComponent,
        DataGeneralComponent,
        ContactProfileComponent,
        CustomizeAvatarComponent,
        DocumentationComponent,
        ManagePasswordsComponent,
        FormChangePasswordsComponent,
        FormChangePasswordsTransaccionsComponent,
        SecurityQuestionsComponent,
        EpinComponent,
    ],
    entryComponents: [
        UpdateGeneralComponent
    ],
    providers: [ProfileService]
})
export class ProfileModule {
}
