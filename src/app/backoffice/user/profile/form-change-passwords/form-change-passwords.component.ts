import { Component, OnInit } from '@angular/core';
import{Router, ActivatedRoute} from '@angular/router';

import {TranslateService} from '@ngx-translate/core';
import {ProfileService} from '../profile.service';

import {DBLocal} from '../../../../core/util/db-local';
import {SWAlert} from '../../../../core/util/swalert';

@Component({
	selector: 'app-change-passwords',
	templateUrl: './form-change-passwords.component.html'
})
export class FormChangePasswordsComponent implements OnInit {

	public storage: DBLocal;
	public alert: SWAlert;

	public input: any;

	constructor(private ps: ProfileService,
				private ts: TranslateService,
                private router: Router) { }

	ngOnInit() {
		this.storage = new DBLocal(true);

		this.input = {'captcha': '','user': '','newpassword': ''};

		this.alert = new SWAlert(this.ts, this.router);
	}

    public sendUpdatePassword(event: any): void {
    	//console.log(this.user.oldpassword, this.user.newpassword, this.storage.GetData('user').username)
      let session = this.storage.SelectQuery('session', ['username']);
      let username = session.rows[0].fields[0].value
		this.ps.updatePassword(this.input.oldpassword, this.input.newpassword, username)
			.subscribe(
				response => this.validateUpdatePassword(response),
				error => this.alert.ServerError()
			);
    }

    public ErrorPassword(event){
    	this.alert.SWError(event.error);
    }

    public validateUpdatePassword(response: any){
        if(response.state >= 1 && response.state <= 100) {
			this.alert.SWAlertSuccessLink("passwordsendemail",['/backoffice/profile']);
		} else {
			this.alert.SWErrorResponse(response);
		}
    }



}
