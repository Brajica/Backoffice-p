import { Component, OnInit } from '@angular/core';
import {DBLocal} from '../../../../core/util/db-local';
import { ProfileService } from '../profile.service';
import { SWAlert } from '../../../../core/util/swalert';
import {Session} from '../../../../core/types/session';
import {  Router, ActivatedRoute, Params } from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
@Component({
	selector: 'app-form-change-passwords-transaccions',
	templateUrl: './form-change-passwords-transaccions.component.html'
})
export class FormChangePasswordsTransaccionsComponent implements OnInit {
	public step: number;
	public alert: SWAlert;
	public display: any;
	public storage: DBLocal;
    public question: number;
    public answer: string;
	public state: number;
	public oldPassword: string;
	public newPassword: string;
	constructor( private ps: ProfileService,
              private route: ActivatedRoute,
	          private ts: TranslateService,
              private router: Router
              ) { }

	ngOnInit() {
	    this.Passwordtransactions();
		this.step = 0;
		this.storage = new DBLocal(true);
		this.alert = new SWAlert(this.ts, this.router);
	}

// funcion que recive los parametros del componente hijo
    public setstep(event: any){
    	this.step = event.step;
    	this.question = event.question;
    	this.answer = event.answer;
    }
    // ejecutamos el metodo con el cual se envian los datos a la api
    public setPassword(event: any){
    	this.oldPassword = event.oldPassword
    	this.newPassword = event.newPassword
    	this.sendPasswordTrans(Session.username,this.oldPassword,this.newPassword,this.question, this.answer);
    }
    
    public Passwordtransactions(){
     this.ps.verificacionPassTrasn(Session.username)
     .subscribe(
       response => this.responsetrans(response)
       );
   }
   public responsetrans(response: any){
	   console.log(response.state)
      this.state = response.state;
   }
// Pasamos los parametros que se van a mandar a la api de cambiar contraseñas de transacciones
  public sendPasswordTrans(username: string, oldPassword:string, newPassword:string, question: number, answer: string){
  	this.ps.sendPasswordTrans(username, oldPassword, newPassword, question, answer)
     .subscribe(
       response => this.responsePass(response)
       );
  }
//   respuesta que se muestra al usuario cuando la api responde
  public responsePass(response){
	  console.log(response)
  	if(response.state >= 1 && response.state <= 100) {
			this.alert.AlertInfo("passwordTransUpdate");
		} else {
			this.alert.SWErrorResponse(response);
		}
  }
}
