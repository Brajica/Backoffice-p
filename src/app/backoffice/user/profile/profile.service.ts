import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { CustomRequests } from '../../../core/util/services';
import { GLOBALS } from '../../../core/config/config';

@Injectable()
export class ProfileService {
    constructor(private http: Http) {
    }

    public getDataGeneral (username: any) {
        const headers = new Headers();

        CustomRequests.CreateAuthorizationHeader(headers);

        const options = new RequestOptions({headers: headers});

        return this.http.post(GLOBALS.URL_BASE + 'user/profile', {"username": username})
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }
    // metodo ejecutado para enviar actualizar la contraseña del backoffice en
    // la cual se envia la vieja y nueva contraseña
    public updatePassword(oldpassword: string, newpassword: string, user: string){
        return this.http.post(GLOBALS.URL_BASE + 'user/update/password/within',{'oldpassword':oldpassword,'newpassword': newpassword, 'username': user}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
    }

    // metodo que se ejecuta despues de
    //  que el usuario abre un link que fue enviado el correo para actualizar contraseña
     public codeUpdate(code: string){
         return this.http.post(GLOBALS.URL_BASE + 'user/confir/password/within',{'code':code}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }
    //actualizacion de contactos del usuario
     public UpdateContact(skype: string, call: string, user: string){
           return this.http.post(GLOBALS.URL_BASE + '/user/update/contactinformation',{'username':user, 'skype': skype, 'whatsapp': call}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }
    // actualizacion de avatar de los que la plataforma da como opcion
    public UpdateAvatar(user: string, avatar: number){
        return this.http.post(GLOBALS.URL_BASE + '/user/update/avatar',{'username': user, 'avatar': avatar}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
    }
        // verificacion de que si el usuario tiene o no tiene contraseña de transacciones
     public verificacionPassTrasn(user: string){
         return this.http.post(GLOBALS.URL_BASE + '/user/get/password/transaction', {'username': user}).
         map(CustomRequests.ResponseData).
         catch(CustomRequests.handleError);
     }
        //actualizacion y/o creacio de contraseñas de transacciones
     public sendPasswordTrans(user: string,oldpassword: string, newpassword: string, question: number, answer: string){
         return this.http.post(GLOBALS.URL_BASE + '/user/update/password/transaction', {'username': user,
                                                                                        'old_password_tran': oldpassword,
                                                                                        'new_password_tran': newpassword,
                                                                                        'ids_question':question,
                                                                                        'response': answer
         }).
         map(CustomRequests.ResponseData).
         catch(CustomRequests.handleError);
     }
        // metodo que obtiene los ultimos afiliados registrados
      public GetAfiliates(){
          return this.http.get(GLOBALS.URL_BASE + '/getlastaffiliates', {}).
         map(CustomRequests.ResponseData).
         catch(CustomRequests.handleError);
      }
    //   metodo que obtiene los paises
      public GetCountries(){
      return this.http.get(GLOBALS.URL_BASE + 'getactivecountries',{}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }
        // actualizacion de los datos que son permitidos para el usuario
     public updateProfileData (data: any) {
       return this.http.post(GLOBALS.URL_BASE + '/user/update/data',{'username': data.username,
                                                                                   'whatsapp': data.whatsapp,
                                                                                   'skype': data.skype,
                                                                                   'address': data.address,
                                                                                   'phone': data.phone,
                                                                                   'city': data.city}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }
        //actualizacion del avatar seleccionado desde el dispositivo del usuario
     public ImgProfile(data: any){
        let formData = new FormData();
        formData.append("avatar", data.file);
        formData.append('username', data.username)
         return this.http.post(GLOBALS.URL_BASE + 'user/upload/avatar', formData).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }
        // actualizacion de la pierna por donde se quieren seguir agregando usuarios
        public updateDirects(data: any){
            return this.http.post(GLOBALS.URL_BASE + 'user/update-direct-binaries',  {'username':data.username, 'side':data.side}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
        }
}
