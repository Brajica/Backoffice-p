import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MdDialog } from '@angular/material';
import {ProfileService} from '../profile.service';
import { UpdateGeneralComponent } from '../../../../forms/profile/update-general/update-general.component';
import {User} from '../../../../core/types/user';

@Component({
  selector: 'app-data-general',
  templateUrl: './data-general.component.html'
})
export class DataGeneralComponent implements OnInit {
  public window_data: any;
    @Input() user: any;

    @Output() general_event = new EventEmitter();
    constructor(private ps: ProfileService,
                public Dialog: MdDialog) { }
   public openDialog() {
      let clas = 'col l4 s12 m12';
      this.Dialog.open(UpdateGeneralComponent, {
        width: '40%',
        data: this.user,
        disableClose: true,
        panelClass: 'm-width'
     });
    }
    ngOnInit() {
    this.window_data = { width: 0 };
    this.ResizeWidth();
    window.addEventListener('resize', () => { this.ResizeWidth(); });
    console.log(this.user)
   }

    public SendWidth() {
    this.general_event.emit({ 'width': this.window_data.width });
  }

  private ResizeWidth(){
    this.window_data.width = document.getElementById('profile-data').offsetWidth;
    this.SendWidth();
  }

}
