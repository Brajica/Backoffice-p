import {Component, OnInit, Input} from '@angular/core';

import {ProfileService} from '../profile.service';
import {SWAlert} from '../../../../core/util/swalert';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {DBLocal} from '../../../../core/util/db-local';
import {User} from '../../../../core/types/user';
import {Session} from '../../../../core/types/session';
@Component({selector: 'app-contact-profile', templateUrl: './contact-profile.component.html'})
export class ContactProfileComponent implements OnInit {
  public storage : DBLocal;
  public alert : SWAlert;

  @Input()user : User;

  constructor(private ps : ProfileService,
              private route : ActivatedRoute,
              private ts : TranslateService,
              private router : Router) {}

  ngOnInit() {
    this.storage = new DBLocal(true);
    this.alert = new SWAlert(this.ts, this.router);
  }

  public sendContact(event : any) {
    this.ps.UpdateContact(this.user.skype, this.user.whatsapp, Session.username)
      .subscribe(response => this.responseContact(response), error => this.alert.ServerError());
  }

  public responseContact(response : any) {
    if (response.state >= 1 && response.state <= 100) {
      Session.whatsapp  = response.data.attributes.whatsapp;
      Session.skype = response.data.attributes.skype;
      this.alert.AlertInfo("contactprofileupdate");
    } else {
      this.alert.SWErrorResponse(response);
    }
  }
}
