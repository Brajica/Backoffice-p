import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import {DBLocal} from '../../../../core/util/db-local';
import {Router} from '@angular/router';
@Component({
  selector: 'app-manage-passwords',
  templateUrl: './manage-passwords.component.html'
})
export class ManagePasswordsComponent implements OnInit {
  public storage: DBLocal;

  constructor(private ps: ProfileService,
              private router: Router
              ) { }

  ngOnInit() {
    this.storage = new DBLocal(true);
  }
}
