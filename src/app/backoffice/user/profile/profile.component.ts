import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {TranslateService} from '@ngx-translate/core';
import {ProfileService} from './profile.service';

import {User} from '../../../core/types/user';
import {DBLocal} from '../../../core/util/db-local'
import {SWAlert} from '../../../core/util/swalert';
import {Session} from '../../../core/types/session';
@Component({selector: 'app-profile', templateUrl: './profile.component.html', providers: [ProfileService]})
export class ProfileComponent implements OnInit {
  private storage : DBLocal;

  private alert : SWAlert;

  public window_data : any;

  public user : Session;

  constructor(private ts : TranslateService,
              private ps : ProfileService,
              private router : Router) {}

  ngOnInit() {
    this.user = new Session();
    this.window_data = {
      'general_data': { width: 0 }
    }
    if (this.user.firstname === '' || this.user.firstname === undefined) {
            this.ps.getDataGeneral(Session.username)
            .subscribe(response => this.dataGeneralSuccess(response),
            error => console.error(error))
      //this.alert = new SWAlert(this.ts, this.router);
       }else{
           console.info(this.user.firstname)
       }
    this.storage = new DBLocal(true);
    this.user = new Session();
    // console.info(Session.username)
  }

  public dataGeneralSuccess(response) {
    console.log(response)
    if (response.state >= 1 && response.state <= 100) {
      this.user.leg           = response.data.attributes.leg;              
      this.user.pack          = response.data.attributes.pack;
      this.user.city          = response.data.attributes.city;
      this.user.phone         = response.data.attributes.phone;
      this.user.email         = response.data.attributes.email;
      this.user.skype         = response.data.attributes.skype;
      this.user.address       = response.data.attributes.address;
      this.user.country       = response.data.attributes.country;
      this.user.lastname      = response.data.attributes.lastname;
      this.user.whatsapp      = response.data.attributes.whatsapp;
      this.user.datelimit     = response.data.attributes.datelimit;
      this.user.firstname     = response.data.attributes.firstname;
      this.user.afiliate_code = response.data.attributes.cod_id;
      this.user.afiliate_date = response.data.attributes.dateafiliation;
    } else {
     console.info(response);
    }
  }

  public getGeneralWidth(event) : void {
    this.window_data.general_data.width = event.width;
    //console.log(this.window_data.general_data.width);
  }
}
