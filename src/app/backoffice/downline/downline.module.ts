import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {DownlineComponent} from './downline.component';

export const routes: Routes = [
    {
        path: '',
        component: DownlineComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DownlineRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        DownlineRoutingModule,
        TranslateModule
    ],
    declarations: [
        DownlineComponent
    ]
})
export class DownlineModule {
}