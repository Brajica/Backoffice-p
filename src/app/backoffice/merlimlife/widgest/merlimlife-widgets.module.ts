import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerlimlifeWidgetsComponent } from './merlimlife-widgets/merlimlife-widgets.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MerlimlifeWidgetsComponent
  ],
  exports: [
    MerlimlifeWidgetsComponent
  ]
})
export class MerlimlifeWidgetsModule { }
