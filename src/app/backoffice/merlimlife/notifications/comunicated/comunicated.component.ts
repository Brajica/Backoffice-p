import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-comunicated',
  templateUrl: './comunicated.component.html',
  animations: [
    trigger('slider', [
      state('*', style({
           transform:'translate(0)'
      })),
      state('1', style({
        opacity:1,
      transform: 'translateX(100%)'
      })),
      state('0', style({
        opacity: 0,
        transform: 'translateX(-100%)',
        display: 'none'
      })),
      state('3', style({
        opacity:1,
        transform: 'translateX(50%)'
      })),
      transition('1 => 0', animate('500ms ease-in')),
      transition('0 => 1', animate('500ms ease-out')),
      transition('* => 0', animate('500ms ease-out')),
      transition('* => 1', animate('500ms ease-in')),
      transition('1 => 3', animate('500ms ease-in'))
    ])
  ]
})
export class ComunicatedComponent implements OnInit {
public state: any = ['2','2','2','0','0'];

  constructor() { }
  ngOnInit() {
  }
  public left(){
     this.state[0] = '0';
     this.state[1] = '3';
     this.state[2] = '3';
     this.state[3] = '1';
 }
 public right(){

 }
}
