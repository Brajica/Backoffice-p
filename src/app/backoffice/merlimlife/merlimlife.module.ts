import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MerlimlifeComponent} from './merlimlife.component';
import { MerlimChannelVideosComponent } from './merlim-channel-videos/merlim-channel-videos.component';
import { CommentsComponent } from './merlim-channel-videos/comments/comments.component';
import { FeaturedVideosComponent } from './merlim-channel-videos/featured-videos/featured-videos.component';
import { RecentVideosComponent } from './merlim-channel-videos/recent-videos/recent-videos.component';
import { EducationSystemComponent } from './education-system/education-system.component';
import { PromotionalMaterialComponent } from './promotional-material/promotional-material.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { CustomizeSiteComponent } from './promotional-material/customize-site/customize-site.component';
import { ComunicatedComponent } from './notifications/comunicated/comunicated.component';
import { UserWidgetsModule } from '../user/widgets/widgets.module';
import {MdButtonModule} from '@angular/material';
import {MdInputModule} from '@angular/material';
export const routes: Routes = [
    {
        path: '',
        component: MerlimlifeComponent
    },
    {
        path: 'channelmerlim',
        component: MerlimChannelVideosComponent
    },
    {
        path: 'education',
        component: EducationSystemComponent
    },
    {
        path: 'promotions',
        component: PromotionalMaterialComponent
    },
    {
        path: 'notifications',
        component: NotificationsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MerlimlifeRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        MerlimlifeRoutingModule,
        TranslateModule,
        UserWidgetsModule,
        MdButtonModule,
        MdInputModule
    ],
    declarations: [
        MerlimlifeComponent,
        MerlimChannelVideosComponent,
        CommentsComponent,
        FeaturedVideosComponent,
        RecentVideosComponent,
        EducationSystemComponent,
        PromotionalMaterialComponent,
        NotificationsComponent,
        CustomizeSiteComponent,
        ComunicatedComponent
    ]
})
export class MerlimlifeModule {
}
