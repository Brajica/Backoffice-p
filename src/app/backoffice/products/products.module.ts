import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {ProductsComponent} from './products.component';

export const routes: Routes = [
    {
        path: '',
        component: ProductsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductsRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        ProductsRoutingModule,
        TranslateModule
    ],
    declarations: [
        ProductsComponent
    ]
})
export class ProductsModule {
}