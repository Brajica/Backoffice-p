import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


import {BackofficeComponent} from './backoffice.component';

export const routes: Routes = [
    {
        path: '',
        component: BackofficeComponent,
        children: [
            {path: 'home', loadChildren: 'app/backoffice/landing/landing.module#LandingModule'},
            {path: 'binary', loadChildren: 'app/backoffice/binary/binary.module#BinaryModule'},
            {path: 'commissions', loadChildren: 'app/backoffice/commissions/commissions.module#CommissionsModule'},
            {path: 'downline', loadChildren: 'app/backoffice/downline/downline.module#DownlineModule'},
            {path: 'merlinlife', loadChildren: 'app/backoffice/merlimlife/merlimlife.module#MerlimlifeModule'},
            {path: 'product', loadChildren: 'app/backoffice/products/products.module#ProductsModule'},
            {path: 'profile', loadChildren: 'app/backoffice/user/profile/profile.module#ProfileModule'},
            {path: 'support', loadChildren: 'app/backoffice/support/support.module#SupportModule'},
            {path: 'unilevel', loadChildren: 'app/backoffice/unilevel/unilevel.module#UnilevelModule'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BackofficeRoutingModule {
}
