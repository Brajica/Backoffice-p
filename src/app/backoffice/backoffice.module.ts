import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {BackofficeRoutingModule} from './backoffice.routing';

import {BackofficeComponent} from './backoffice.component';

@NgModule({
    imports: [
        CommonModule,
        BackofficeRoutingModule,
        NgbModule,
        TranslateModule
    ],
    declarations: [
        BackofficeComponent
    ]
})
export class BackofficeModule {
}
