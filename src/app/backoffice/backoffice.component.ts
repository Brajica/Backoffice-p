import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SessionService} from '../core/services/session.service';
import {TranslateService} from '@ngx-translate/core';

import {Session} from '../core/types/session';
import {DBLocal} from '../core/util/db-local';

@Component({selector: 'app-home', templateUrl: './backoffice.component.html', providers: [SessionService], styleUrls: ['./backoffice.component.css']})
export class BackofficeComponent implements OnInit {

    public storage : DBLocal;
    public sub_menu_indicador = {
        indicador_1: false
    };

    public user : Session;

    constructor(private ss : SessionService, private ts : TranslateService, private router : Router) {}

    ngOnInit() {
        // this.ss.LogoutCheck();

        this.ts.use(localStorage.getItem('lang'));

        if (document.body.classList.contains('register')) {
            document.body.classList.remove('register');
        }

        this.storage = new DBLocal(true);

        this.user = new Session();

        try {
            const result = this.storage.SelectQuery('session', ['username', 'avatar', 'level', 'range', 'date']);

            console.log('--result-data-storage');
            console.log(result);
            for (let a = 0; a < result.rows[0].fields.length; a++) {
                this.user[result.rows[0].fields[a].name] = result.rows[0].fields[a].value;
                // console.log(Session[result.rows[0].fields[a].name])
            }

            console.log('--session-recover-success');
            console.log(Session);

        } catch (e) {
            console.error(e);
            sessionStorage.clear();
            localStorage.clear();
            this.router.navigate(['/login']);
        }

        this.ResizeEvent();

        window.addEventListener('resize', this.ResizeEvent);
    }

    public Logout() {
        this.ss.Logout(Session.username).subscribe(response => this.LogoutSuccess(response), error => this.LogoutError(error));
    }

    private LogoutSuccess(response : any) {
        console.log('--Logout-data');
        console.log(response);
        if (response.state >= 1 && response.state <= 100) {
            sessionStorage.clear();
            localStorage.clear();
            this.router.navigate(['/login']);
        } else {
            sessionStorage.clear();
            localStorage.clear();
            this.router.navigate(['/login']);
        }
    }

    private LogoutError(response : any) {
        console.log(response);
        alert('server error');
    }

    private ResizeEvent() {

        const menu = document.getElementById('navbar-left');

        menu.style.minHeight = window.innerHeight + 'px';
    }

    public MenuActive() {
        document.getElementById('navbar-left').classList.toggle('active');
    }

    public LangChange(lang : string) {
        localStorage.setItem('lang', lang);
        this.ts.use(lang);
    }

    public clickSubMenu(indicador : string) {
        if (indicador == '0') {
            for (let key in this.sub_menu_indicador) {
                this.sub_menu_indicador[key] = false;
            }
        } else {
            for (let key in this.sub_menu_indicador) {
                if (key == 'indicador_' + indicador) {
                    this.sub_menu_indicador[key] = !this.sub_menu_indicador[key];
                } else {
                    this.sub_menu_indicador[key] = false;
                }
            }
        }
    }
}
