import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {UnilevelComponent} from './unilevel.component';

export const routes: Routes = [
    {
        path: '',
        component: UnilevelComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UnilevelRoutingModule {
}


@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        UnilevelRoutingModule,
        TranslateModule
    ],
    declarations: [
        UnilevelComponent
    ]
})
export class UnilevelModule {
}