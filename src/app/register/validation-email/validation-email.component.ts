import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {RegisterService} from '../register.service';

import {GLOBALS} from '../../core/config/config';

import {SWAlert} from '../../core/util/swalert';
import {User} from '../../core/types/user';

declare var particlesJS: any;

@Component({
    selector: 'app-validation-email',
    templateUrl: './validation-email.component.html',
    styleUrls: ['../register.component.css']
})
export class ValidationEmailComponent implements OnInit {

    private alert: SWAlert;

    public user: User;

    public input_sponsor_editable = true;

    constructor(private ts: TranslateService,
                private rs: RegisterService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {

        if (localStorage.getItem('lang') == null) {
            localStorage.setItem('lang', 'en');
        }

        particlesJS.load('content-particle', GLOBALS.URL_BASE_FONTEND + 'assets/config/particlesjs-config.json');

        this.ts.use(localStorage.getItem('lang'));

        this.user = {};
        this.user.sponsor = '';
        this.user.email = '';
        this.user.confirm_email = '';
        this.user.captcha = '';

        this.route.params.subscribe((params: Params) => {
            if (typeof (params['sponsor']) !== 'undefined') {
                this.user.sponsor = params['sponsor'];
                this.input_sponsor_editable = false;
            }
        });

        document.getElementById('content-form').classList.add('width-d1');

        this.alert = new SWAlert(this.ts, this.router);
    }

    public OnSubmit(event: any): void {
        if (typeof (event.error) === 'undefined') {
            this.rs.ValidateEmail(this.user).subscribe(
                response => this.ResEnviarSuccess(response),
                error => this.alert.ServerError()
            );
        } else {
            this.alert.SWError(event.error);
        }
    }

    private ResEnviarSuccess(res) {
        if (res.state >= 1 && res.state <= 100) {
            this.alert.SWAlertSuccessLink('emailvalidatesuccess', ['/']);
        } else {
            this.alert.SWErrorResponse(res);
        }
    }
}
