import {Component, OnInit} from '@angular/core';

import {SessionService} from '../core/services/session.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    providers: [SessionService],
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    constructor(private ts: TranslateService,
                private ss: SessionService) {
    }

    ngOnInit() {

        if (localStorage.getItem('lang') == null) {
            localStorage.setItem('lang', 'en');
        }

        this.ts.use(localStorage.getItem('lang'));

        // this.ss.LoginCheck();

        window.addEventListener('resize', () => {
            this.onResize();
        });

        setInterval(() => {
            this.onResize();
        }, 0);

    }



    public onResize() {
        const window_h = window.innerHeight;
        const element_h = document.getElementById('content-form').offsetHeight;
        document.getElementById('content-form').style.marginTop = Math.max(0, (window_h / 2) - (element_h / 2) - 50) + 'px';
    }

    // cambia el lenguaje
    public langChange(lang: string) {
        localStorage.setItem('lang', lang);
        this.ts.use(lang);
    }

}
