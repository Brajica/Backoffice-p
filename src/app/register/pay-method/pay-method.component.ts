import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {TranslateService} from '@ngx-translate/core';

import {GLOBALS} from '../../core/config/config';

import {SWAlert} from '../../core/util/swalert';

declare var particlesJS: any;

@Component({
    selector: 'app-pay-method',
    templateUrl: './pay-method.component.html'
})
export class PayMethodComponent implements OnInit {

    private alert: SWAlert;

    private user: string;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private ts: TranslateService) {
    }

    ngOnInit() {
        document.getElementById('content-form').classList.remove('width-d1');
        document.getElementById('content-form').classList.remove('width-d2');
        particlesJS.load('content-particle',  GLOBALS.URL_BASE_FONTEND + 'assets/config/particlesjs-config.json');

        if (localStorage.getItem('lang') == null) {
            localStorage.setItem('lang', 'en');
        }
        this.ts.use(localStorage.getItem('lang'));

        this.alert = new SWAlert(this.ts, this.router);

        this.route.params.subscribe(params => {
            if (params['user'] != null) {

                this.user = params['user'];

            } else {

                let emailerror = '';

                this.ts.get('Errors.userinvalid').subscribe((error: string) => {
                    emailerror = error;
                });

                this.alert.SWAlertConfirm(
                    'alertinputuser',
                    (usuario) => {
                        return new Promise((resolve, reject) => {
                            setTimeout(function () {
                                if (usuario !== null && usuario !== '') {
                                    resolve();
                                } else {
                                    reject(emailerror);
                                }
                            }, 2000);
                        });
                    }, (usuario) => {
                        this.user = usuario;
                    });
            }
        });

    }

    public Epin() {
        if (this.user !== null && this.user !== '') {
            this.router.navigate(['/register/pay-method', this.user, 'epin']);
        }
    }

    public Areto() {
        if (this.user !== null && this.user !== '') {
            this.router.navigate(['/register/pay-method', this.user, 'areto']);
        }
    }

}
