import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Session} from '../../../core/types/session';
import {TranslateService} from '@ngx-translate/core';
import {PayService} from '../../../core/services/pay.service';

import {SWAlert} from '../../../core/util/swalert';


@Component({
    selector: 'app-epin',
    templateUrl: './epin.component.html',
    providers: [PayService],
    styleUrls: ['../../register.component.css']
})
export class EpinComponent implements OnInit {
    public user: Session;
    private alert: SWAlert;

    constructor(private ps: PayService,
                private ts: TranslateService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        if (localStorage.getItem('lang') == null) {
            localStorage.setItem('lang', 'en');
            
        }
        this.user = new Session();
        this.ts.use(localStorage.getItem('lang'));

        this.alert = new SWAlert(this.ts, this.router);
    }
    public Send(event: any): void {
        console.info(this.user)
         if(this.user.username != undefined || this.user.username != null){
            this.SendUpgrade(event);
        }else{
            if (event.key !== '' && event.code !== '') {
                this.route.params.subscribe(params => {
                    if (params['user'] !== null) {
                        this.ps.Epin(event.code, event.key, event.captcha, event.username).subscribe(
                            response => this.EpinSuccess(response),
                            error => this.alert.ServerError()
                        );
                    }
                });
            }
        }
    }
   public SendUpgrade(event: any){
            this.route.params.subscribe(params => {
                if (params['pack'] !== null) {
                    let pack = params['pack'];
                    this.ps.EpinUpgrade({'username':this.user.username, 'pack': pack, 'code': event.code, 'key':event.key }).subscribe(
                        response => this.EpinUpgradesuccess(response),
                        error => this.alert.ServerError()
                    );
                }
            });
   }
    private EpinSuccess(res: any) {
        if (res.state >= 1 && res.state <= 100) {
            this.alert.SWAlertSuccessLink('epinsuccess', ['/login']);
        } else {
            this.alert.SWErrorResponse(res);
        }
    }
    public EpinUpgradesuccess(response: any){
      console.info(response)
    }
}
