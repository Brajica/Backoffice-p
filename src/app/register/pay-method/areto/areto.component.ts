import {Component, OnInit} from '@angular/core';
import {Areto} from '../../../core/types/areto';
import {SWAlert} from '../../../core/util/swalert';
import {AretoService} from '../../../core/services/areto.service';
import {TranslateService} from '@ngx-translate/core';
import {Router, ActivatedRoute,  Params} from '@angular/router';
@Component({
    selector: 'app-areto',
    templateUrl: './areto.component.html',
    providers: [AretoService],
    styleUrls: ['./areto.component.css']
})
export class AretoComponent implements OnInit {

    public areto: Areto;
    public alert : SWAlert;
    constructor(private areto_service: AretoService,
                private route: ActivatedRoute,
                private ts: TranslateService,
                private router: Router) {
    }

    ngOnInit() {
        this.areto = new Areto().Initialize();
        this.alert = new SWAlert(this.ts, this.router)
        this.route.params.subscribe(params => {
            console.log(params);
            if (params['user'] !== null) {
                this.areto_service.getDataAreto(params['user']).subscribe(
                    response => this.getDataAretoSuccess(response),
                    error => this.getDataAretoError(error)
                    // complete => {}
                );
            }
        });
    }

    private getDataAretoSuccess(response: any) {
        console.log(response.data);
        if (response.state >= 1 && response.state <= 100) {
            this.areto.data = response.data.attributes;
            // console.info(this.areto.data);
        } else {

        }
    }

    private getDataAretoError(error) {
        console.log(error);
    }

    public receiveparams(event: any) {
    // console.info(this.areto.data.city)
        this.areto_service.sendData({
            'expiration_date': event.expirate_date,
            'card_number': event.card_number,
            'cvc_code': event.cvc_code,
            'titular': event.titular,
            'postal_code': event.postal_code,
            'package_cost': this.areto.data.package_cost,
            'system_activation': this.areto.data.system_activation,
            'total_package': this.areto.data.total_package,
            'currency_code': this.areto.data.currency_code,
            'street': this.areto.data.street,
            'desc': this.areto.data.desc,
            'city': this.areto.data.city,
            'country_code': this.areto.data.country_code
        }).subscribe(
            response => this.succesinput(response),
            error => this.alert.ServerError()
        );
    }

    private succesinput(response: any) {
        if (response.state >= 1 && response.state <= 100) {
            this.alert.AlertInfo('hfkdsh')
        } else {
            console.info(response.errors[0].process_payment[0])
           this.alert.SWError(response.errors[0].process_payment[0])
        }
    }

    private errorinput(response: any) {
        if (response.state >= 1 && response.state <= 100) {
            console.log(response)
        } else {
            console.log(response)
        }
    }

}
