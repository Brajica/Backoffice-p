/*
 * sub modulo de registro
 * se encarga de cargar todos los componentes y modulos que nesesitara la ruta
 * '/register' y  sus subrutas
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';

import {RegisterService} from './register.service';

import {RegisterRoutingModule} from './register.routing';

/*
 * este modulo se debe cargar en todos los modulos que usen algun formulario ya
 * que en el estan todos los formularios
 */
import {CustomFormsModule} from '../forms/forms.module';

import {RegisterComponent} from './register.component';
import {ValidationEmailComponent} from './validation-email/validation-email.component';
import {CompleteRegisterComponent} from './complete-register/complete-register.component';
import {PayMethodComponent} from './pay-method/pay-method.component';
import {EpinComponent} from './pay-method/epin/epin.component';
import {AretoComponent} from './pay-method/areto/areto.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        TranslateModule,
        CustomFormsModule,
        RegisterRoutingModule
    ],
    declarations: [
        RegisterComponent,
        ValidationEmailComponent,
        CompleteRegisterComponent,
        PayMethodComponent,
        EpinComponent,
        AretoComponent
    ],
    providers: [RegisterService]
})
export class RegisterModule {
}
