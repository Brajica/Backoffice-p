/*
 * subrutas de la ruta 'register'
 */

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

/*
 * importamos componentes que nesesitaremos
 */
import {RegisterComponent} from './register.component';
import {ValidationEmailComponent} from './validation-email/validation-email.component';
import {CompleteRegisterComponent} from './complete-register/complete-register.component';
import {PayMethodComponent} from './pay-method/pay-method.component';
import {EpinComponent} from './pay-method/epin/epin.component';
import {AretoComponent} from './pay-method/areto/areto.component';

export const routes: Routes = [
    {
        path: '',
        component: RegisterComponent,
        children: [
            {path: '', redirectTo: 'validate-email', pathMatch: 'full'},
            {path: 'validate-email', component: ValidationEmailComponent},
            {path: 'validate-email/:sponsor', component: ValidationEmailComponent},
            {path: 'complete/:code', component: CompleteRegisterComponent},
            {path: 'pay-method', component: PayMethodComponent},
            {path: 'pay-method/:user', component: PayMethodComponent},
            {path: 'pay-method/:user/epin', component: EpinComponent},
            {path: 'pay-method/:user/areto', component: AretoComponent}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RegisterRoutingModule {
}
