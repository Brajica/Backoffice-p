import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {CustomRequests} from '../core/util/services';
import {GLOBALS} from '../core/config/config';

@Injectable()
export class RegisterService {
    public lang = localStorage.getItem('lang');
    public static url_base = GLOBALS.URL_BASE;

    constructor(private http: Http) {
    }

    public ValidateEmail (input: any) {
        // const user = { 'sponsor': sponsor, 'email': email, 'captcha': captcha };
        console.log(input);
        return this.http.post(RegisterService.url_base + this.lang +  '/register/' + 'validatemail', input)
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }

    public GetInstanceRegister(code) {
        return this.http.post(RegisterService.url_base +  'register/' +  'getinstance', {'code': code})
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }

    public send(user:any) {
        return this.http.post(RegisterService.url_base +  'register/' + 'save', user)
            .map(CustomRequests.ResponseData)
            .catch(CustomRequests.handleError);
    }
    public aretoSend(){
        return this.http.post(RegisterService.url_base +  'register/' +  '/paid/processpayment', {})
        .map(CustomRequests.ResponseData)
        .catch(CustomRequests.handleError);
    }
}
