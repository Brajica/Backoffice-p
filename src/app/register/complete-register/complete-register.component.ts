import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {GLOBALS} from '../../core/config/config';

import {RegisterService} from '../register.service';
import {SWAlert} from '../../core/util/swalert';
import {User} from '../../core/types/user';

declare var particlesJS: any;

@Component({
    selector: 'app-complete-register',
    templateUrl: './complete-register.component.html',
    styleUrls: ['./complete-register.component.css']
})
export class CompleteRegisterComponent implements OnInit {

    private alert: SWAlert;

    public mainstep: number;

    public user: User;

    constructor(private rs: RegisterService,
                private router: Router,
                private ts: TranslateService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {

        if (localStorage.getItem('lang') == null) {
            localStorage.setItem('lang', 'en');
        }

        particlesJS.load('content-particle', GLOBALS.URL_BASE_FONTEND + 'assets/config/particlesjs-config.json');

        this.alert = new SWAlert(this.ts, this.router);

        this.mainstep = 1;

        this.user = {};
        this.user.code = '';
        this.user.sponsor = '';
        this.user.username = '';
        this.user.password = '';
        this.user.email = '';
        this.user.firstname = '';
        this.user.lastname = '';
        this.user.birth_date = ''; // { 'year': 2017, 'month': 1, 'day': 1};
        this.user.country = {};
        this.user.city = '';
        this.user.address = '';
        this.user.document_type = '';
        this.user.document_number = '';
        this.user.plan = 0;
        this.user.captcha = '';
        this.route.params.subscribe(params => {
            if (params['code'] !== null) {
                this.user.code = params['code'];
                // this.user.sponsor = 'default';
                // this.user.email = 'default@default.default';
                this.rs.GetInstanceRegister(params['code']).subscribe(
                    response => this.InstanceRegisterSuccess(response),
                    error => this.alert.ServerError()
                );
            }
        });
        document.getElementById('content-form').classList.add('width-d1');
        if (this.mainstep >= 3) {
            document.getElementById('content-form').classList.remove('width-d1');
            document.getElementById('content-form').classList.add('width-d2');
        } else {
            document.getElementById('content-form').classList.remove('width-d2');
            document.getElementById('content-form').classList.add('width-d1');
        }
    }

    private InstanceRegisterSuccess(response: any) {
        if (response.state >= 1 && response.state <= 100) {

            this.user.sponsor = response.data.attributes.sponsor;
            this.user.email = response.data.attributes.email;

        } else {

            this.alert.SWErrorLink('linkinvalid', ['/register/validate-email']);

        }
    }

    public next(event: any): void {
        if (event.action === 'next') {
            if(this.user.country !== '' || this.user.document_type !== '') {
                this.mainstep++;
            } else {
                this.alert.SWError('inputrequire');
            }
        } else if (event.action === 'back') {
            this.back(event.numinterval);
        } else if (event.action === 'selectpack') {
            this.user.plan = event.pack;
            this.mainstep++;
        } else if (event.action === 'send') {
            this.send();
        }

        if (this.mainstep >= 3) {
            document.getElementById('content-form').classList.remove('width-d1');
            document.getElementById('content-form').classList.add('width-d2');
        } else {
            document.getElementById('content-form').classList.remove('width-d2');
            document.getElementById('content-form').classList.add('width-d1');
        }

    }

    private back(num: number) {
        if (this.mainstep > 1) {
            this.mainstep -= num;
        }
    }

    public errors(event: any): void {
        if (typeof (event.error) !== 'undefined') {
            if (event.error === 'servererror') {
                this.alert.ServerError();
            } else {
                this.alert.SWError(event.error);
            }
        }
    }

    public send() {
      // console.info(this.user)
        this.rs.send(this.user)
            .subscribe(
                response => this.SendSuccess(response),
                error => this.alert.ServerError()
            );
    }

    private SendSuccess(response: any) {
        console.log(response);
        if (response.state) {
            this.alert.SWAlertSuccessLink('registersuccess', ['/register/pay-method', this.user.username]);
        } else {
            this.alert.SWErrorResponse(response);
        }
    }

}
