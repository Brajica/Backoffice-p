import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { TestService } from './test.service';
import { RouterModule, Routes } from '@angular/router';
import { TestComponent } from './test.component';

// declaramos las rutas
// en las rutas las variables se declaran de la manera "ruta/:variable/ruta"
export const routes: Routes = [
    {path: '', component: TestComponent}
];

@NgModule({
  imports: [
    CommonModule,
    AmChartsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TestComponent],
  providers: [
        TestService 
  ]
})
export class TestModule { }
