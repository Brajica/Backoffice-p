import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { CustomRequests } from '../core/util/services';
import { GLOBALS } from '../core/config/config';
@Injectable()
export class TestService {

  constructor(private http: Http) { }
  
  public GetCountries(){
      return this.http.get(GLOBALS.URL_BASE + 'getaffiliatesforcountries',{}).
            map(CustomRequests.ResponseData).
            catch(CustomRequests.handleError);
     }
  }


