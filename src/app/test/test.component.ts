import {  Component,  OnInit} from '@angular/core';
import {  AmChartsService} from "@amcharts/amcharts3-angular";
import { TestService } from './test.service';
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html'
})
export class TestComponent implements OnInit {
  private chart: any;
  public countries: any = [];
  constructor(private AmCharts: AmChartsService, private ts: TestService) {}

  ngOnInit() {
    this.getCountries();
  }
  
  private getCountries(){
    this.ts.GetCountries()
			.subscribe(
				response => this.succesCountries(response),
				error => this.errorCountries(error)
			);
  }
  
  private succesCountries(response: any){
    for (let i in response.data){
      let item = {"title": response.data[i].country_master_name +": " +response.data[i].amount ,
                  "id": response.data[i].CODE,
                  "selectable": false }
      this.countries.push(item)
    }
    console.info(response.data)
    this.chart = this.AmCharts.makeChart("chartdiv", {
      "type": "map",
      "theme": "light",
      "dataProvider": {
        "map": "worldLow",
        "areas": this.countries
      },
      "areasSettings": {
        "color": "#55DAF1",
        "selectedColor": "#0C0D0D"
      },
      "backgroundAlpha": 1,
      "backgroundColor": "#1f80c1"
    });
    this.chart.path = "/node_modules/amcharts3/amcharts/";
    
  }
  private errorCountries (response: any) {
    console.error(response.errors)
  }

}
