import 'hammerjs';

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HttpModule, Http} from '@angular/http';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

/*
 * sistema multilenguaje
 * url: https://www.npmjs.com/package/ng2-translate
 */
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {RecaptchaModule} from 'ng-recaptcha';
import {AmChartsModule} from "@amcharts/amcharts3-angular";

import {AppRoutingModule} from './app.routing';

import {AppComponent} from './app.component';


/*
 * es necesario exportar esta funcion para el sistema multilenguaje
 */
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        AmChartsModule,
        RecaptchaModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
