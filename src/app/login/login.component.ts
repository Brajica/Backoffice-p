import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {SessionService} from '../core/services/session.service';

import {GLOBALS} from '../core/config/config';

import {default as swal} from 'sweetalert2';
import {SWAlert} from '../core/util/swalert';
import {DBLocal} from '../core/util/db-local';
import {Session} from '../core/types/session';
import {User} from '../core/types/user';

declare var particlesJS: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    providers: [SessionService],
    styleUrls: ['./login.component.css', '../../assets/css/materialize/materialize-shadows.css']
})
export class LoginComponent implements OnInit {

    private alert: SWAlert;
    private storage: DBLocal;

    public user: User;

    constructor(private router: Router, private ss: SessionService, private ts: TranslateService){}

    ngOnInit()
    {
        // this.ss.LoginCheck();
        particlesJS.load('content-particle', GLOBALS.URL_BASE_FONTEND + 'assets/config/particlesjs-config.json');

        if (localStorage.getItem('lang') == null)
        {
            localStorage.setItem('lang', 'en');
        }

        this.ts.setDefaultLang(localStorage.getItem('lang'));

        // Inicializamos variables
        this.alert = new SWAlert(this.ts, this.router);
        this.storage = new DBLocal(true);

        this.user = {};
        this.user.username = '';
        this.user.password = '';
        this.user.captcha = '';

        setTimeout(() => {
            this.ts.get('General').subscribe((general: any) => {
                this.ts.get('InfoSupport').subscribe((info: any) => {
                    swal({
                        title: info.title1,
                        text: info.renovatepassword3.text,
                        type: 'info',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: info.renovatepassword3.btn_change_password,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        cancelButtonText: info.renovatepassword3.btn_renew,
                        confirmButtonClass: 'btn btn_alert_1 mb-8 pink darken-3 white-text',
                        cancelButtonClass: 'btn btn_alert_1 pink darken-3 white-text',
                        buttonsStyling: false
                    }).then(() => {
                        console.log('send mesage');
                    }, (dismiss) => {
                        // dismiss can be 'cancel', 'overlay',
                        // 'close', and 'timer'
                        this.router.navigate(['/recover-password']);
                    });
                });
            });
        }, 0);

        window.addEventListener('resize', () => { this.onResize(); });
        setTimeout(() => { this.onResize(); }, 1000);
    }

    public onResize()
    {
        const window_h = window.innerHeight;
        const element_h = document.getElementById('login-content').offsetHeight;
        document.getElementById('content-particle').style.height = (window_h - 20) + 'px';
        document.getElementById('login-content').style.marginTop = Math.max(0, (window_h / 2) - (element_h / 2)) + 'px';
    }

    public send(event: any)
    {
        if (event)
        {
            document.getElementById('loading').classList.remove('hidden');
            // console.log('--data-send-login')
            // console.log(this.user);
            this.ss.Login(this.user).subscribe(
                response => this.loginSuccess(response),
                error => () => { this.alert.ServerError(); document.getElementById('loading').classList.add('hidden'); }
            );
        }
    }

    private loginSuccess(res: any)
    {
        // console.log('--data-seccess-login');
        // console.log(res);
        if (res.state >= 1 && res.state <= 100)
        {
            // console.log(res.data);
            localStorage.setItem('csrf_token', res.data.attributes.token);
            this.storage.expire_in = 120;
            this.storage.InsertQuery(
                'session',
                [
                    'username',
                    'avatar',
                    'level',
                    'range',
                    'date'
                ],
                [
                    res.data.attributes.username,
                    res.data.attributes.avatar,
                    res.data.attributes.level,
                    res.data.attributes.range,
                    res.data.attributes.date
                ]
            );
            Session.clearSegurity();

            let user = new Session();

            user.username = res.data.attributes.username;
            user.avatar = res.data.attributes.avatar;
            user.level = res.data.attributes.level;
            user.range = res.data.attributes.range;
            user.date = res.data.attributes.date;

            // console.log('--data-seccess-session');
            // console.log(Session);

            document.getElementById('loading').classList.add('hidden');
            this.router.navigate(['/backoffice/home']);
        }
        else
        {
            document.getElementById('loading').classList.add('hidden');
            this.alert.SWErrorResponse(res);
        }
    }

    public langChange(lang: string)
    {
        localStorage.setItem('lang', lang);
        this.ts.use(lang);
    }
}
