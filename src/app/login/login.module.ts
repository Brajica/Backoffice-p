import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';

import {LoginRoutingModule} from './login.routing';
import {CustomFormsModule} from '../forms/forms.module';
import {RecaptchaFormsModule} from 'ng-recaptcha/forms';

import {SessionService} from '../core/services/session.service';
import { MdButtonModule } from '@angular/material';
import {LoginComponent} from './login.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        TranslateModule,
        CustomFormsModule,
        LoginRoutingModule,
        MdButtonModule
    ],
    declarations: [
        LoginComponent
    ],
    providers: [SessionService]
})
export class LoginModule {
}
