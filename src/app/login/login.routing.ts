import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {LoginComponent} from './login.component';

// declaramos las rutas
// en las rutas las variables se declaran de la manera "ruta/:variable/ruta"
export const routes: Routes = [
    {path: '', component: LoginComponent}
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule {
}